# def zsq(fn):
# 	def baby():
# 		print('满江红')
# 		fn()
# 		print('岳飞')
# 	return baby

# # 作用：用zsq这个装饰器去装饰bzs这个函数，装饰完以后，再用bzs这个函数的时候，就是加强版的bzs函数
# @zsq
# def bzs():
# 	print('怒发冲冠凭栏处,潇潇雨歇抬望眼')

# # bzs()
# # bzs = zsq(bzs)
# # bzs()


# # 带参数的装饰器
# def zhuangshiqi(fn):
# 	def demo(name, age):
# 		print('xxxxxxxxx')
# 		fn(name, age)
# 		print('yyyyyyyyy')
# 	return demo
# @zhuangshiqi
# def beizhuangshi(name, age):
# 	print('%s是一个非常专一的男生,他只喜欢%s年龄的女孩' % (name, age))

# beizhuangshi = zhuangshiqi(beizhuangshi)
# beizhuangshi('杨玉洁', '18')


# 带返回值的装饰器
# def zhuangshiqi(fn):
# 	def demo(name, like):
# 		print('xxxxxxxx')
# 		ret = fn(name, like)
# 		print('yyyyyyyy')
# 		return ret + 'haha'
# 	return demo

# @zhuangshiqi
# def beizhuangshi(name, like):
# 	print('我叫%s,我喜欢%s' % (name, like))
# 	return name + '爱=====>' + like

# 函数被装饰完之后，调用的格式和以前一模一样
# 以前有参数，有返回值，现在还有参数，还有返回值
# ret = beizhuangshi('迪丽热巴', '斌哥')
# print(ret)


# def test(*args, **argv):
# 	print(args)
# 	print(argv)

# test(1, 2, 3, 4, name='goudan', age='18')


# # 通用装饰器
def zhuangshiqi(fn):
	def demo(*args, **argv):
		print('xxxxxxxxxxxxxxx')
		ret = fn(*args, **argv)
		print('yyyyyyyyyyyyyyy')
		return ret
	return demo

@zhuangshiqi
def beizhuangshi(name, age, like, like1):
	print('我的名字叫做%s,我的年龄为%s,我喜欢%s,我更喜欢%s' % (name, age, like, like1))
	return '霍元甲'


print(beizhuangshi('刘德华', '60', '关之琳', like1='梅艳芳'))
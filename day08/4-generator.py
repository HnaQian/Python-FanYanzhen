lt = (x for x in range(1, 10))
# for x in lt:
# 	print(x)

# for x in lt:
# 	print(lt)

# python2里面
# 	range()  xrange()
# python3里面
# 	range()

# lt1 = list(lt)
# print(lt1)
print(lt)
# print(next(lt))
# print(next(lt))
# print(next(lt))
# print(next(lt))
# print(next(lt))
# print(next(lt))
# print(next(lt))
# print(next(lt))
# print(next(lt))
# print(next(lt))


def lala():
	for x in range(1, 10):
		yield x
	yield 100

g = lala()
# lt = list(g)
# print(lt)


print(next(g))
print(next(g))
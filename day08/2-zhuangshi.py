def demo():

	# start()

	# 外号：老二、三皮、狮子、光头
	print('人缘好的人一般性格都非常open')


	# end()

def start():
	print('我是周杰伦')

def end():
	print('我喜欢昆凌')


# demo()
# 需求变了，需要给demo增加功能
# 第一个方案, 会被开除
# print('我叫周杰伦')
# demo()
# print('我喜欢昆凌')

# 第二种方案，又被开除
# demo()

# 第三种方案, 你是学python的，你走吧，
# 交个朋友，我来教你怎么写
# demo()

# 第四种方案，装饰器
def hehe(fn):
	def xixi():
		print('作者是杜牧')
		fn()
		print('写的时候思想不健康')
	return xixi


def haha():
	print('停车坐爱枫林晚,霜叶红于二月花')

def lala():
	print('lala函数')


haha = hehe(haha)
# print(a)
haha()

def hello():
	print('我喜欢少数民族的女孩')
	def world():
		print('迪丽热巴就不错')

	return world

# a = hello()
# print(a())
# hello()()

def hello(*args):
	def world():
		# 局部变量args在内部函数中可以使用，但是不能修改
		he = 0
		for x in args:
			he += x
		return he
	return world

# a = hello(100, 200, 300, 400, 500)
# print(a())

def hello(a, b):
	def func(x):
		return a*x + b
	return func

func1 = hello(2, 5)  # 2*x + 5
func2 = hello(3, 4)  # 3*x + 4

# print(func1(2))
# print(func2(2))
from collections import Iterator
from collections import Iterable

g = (x for x in range(1, 10))
lt = [1, 2, 3, 4]
# l = iter(lt)
# print(isinstance(g, Iterator))
# print(isinstance(l, Iterator))
print(isinstance(lt, Iterable))
# 第二题
def my_index(lt, obj):
	# 求出列表的长度
	length = len(lt)
	i = 0
	while i < length:
		if obj == lt[i]:
			return i
		i += 1
	else:
		# 如果没有找到，返回-1
		return -1

# lt = ['成龙', '房祖名', '周星驰', '林俊杰', '房祖名']
# print(my_index(lt, '房祖名'))

# 第四题
def string2dict(string):
	# name=goudan&age=18&pwd=123&height=180
	# 将字符串按照 & 符号进行切割
	lt = string.split('&')
	# 先定义一个空字典，用来在后面使用
	dic = {}
	# print(lt)
	# 遍历列表，依次的处理每一个元素
	for item in lt:
		# 将item按照 = 号进行切割
		item_list = item.split('=')
		# 将列表中第一个元素作为键，第二个元素作为值放入到字典中
		dic[item_list[0]] = item_list[1]
	return dic

# print(string2dict('name=goudan&age=18&pwd=123&height=180'))

# 第五题
def dict2string(dic):
	# {'name':'goudan', 'age':'18', 'pwd':'123', 'height':'180' }
	lt = []
	for k, v in dic.items():
		lt.append(k + '=' + v)
	return '&'.join(lt)

	# string = ''
	# 同时遍历键值
	# for k, v in dic.items():
	# 	ret = k + '=' + v + '&'
	# 	string += ret
	# # print(string)
	# return string.rstrip('&')

# print(dict2string({'name':'goudan', 'age':'18', 'pwd':'123', 'height':'180' }))

# print('123a'.isdecimal())

# 第七题
# a%&bc1234lala12hah%$a56
# a = 'hello'
# a[0] = 'a'   # 不能这么写
# a = 'baby'   # 可以这么写
def qiuhe(string):
	# 首先定义一个字符串，用来保存第一次遍历之后的结果，就是将所有的非数字字符替换为  =
	ret = ''
	for x in string:
		# 不是数字字符，将x变为 =号，是数字字符不用动，添加到新的字符串后面就行了
		if not x.isdecimal():
			x = '='
		ret += x
	# print(ret)
	# 将ret字符串按照 = 号进行切割
	lt = ret.split('=')
	# print(lt)
	# 因为lt中有好多的空字符串，需要将这些空字符串过滤掉
	my_sum = 0
	for x in lt:
		# 判断x是不是 非空 字符串
		if x:
			my_sum += int(x)
	return my_sum

# print(qiuhe('a%&bc12lala12hah12%$a12'))
import random
def code(number):
	# 生成数字字符列表
	number_list = [str(i) for i in range(10)]
	# 生成小写字母列表
	little_list = [chr(i) for i in range(97, 123)]
	# 生成大写字母列表
	upper_list = [chr(i) for i in range(65, 91)]
	# 生成总列表
	lt = number_list + little_list + upper_list
	# print(lt)
	ret = ''
	for x in range(number):
		ret += random.choice(lt)

	return ret

print(code(4))

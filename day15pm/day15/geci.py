class LrcManager(object):
	def __init__(self, path):
		self.path = path
		# 存放所有歌词的列表
		self.lrc_list = []
		self.parse()

	# 解析歌词的函数
	def parse(self):
		fp = open(self.path, 'r', encoding='utf8')
		string = fp.read()
		fp.close()

		# 按照换行符进行切割
		lines_list = string.split('\n')
		# 遍历这个列表，依次处理每一句歌词
		for line in lines_list:
			# [02:05.33][00:29.50]鸳鸯双栖蝶双飞
			lt = line.split(']')
			# [02:05.33   [00:29.50   鸳鸯双栖蝶双飞
			info = lt[-1]
			# 提取所有的时间
			time_list = lt[:-1]
			# 遍历时间，得到时间信息
			for tm in time_list:
				tm = tm.lstrip('[')
				# 将tm按照 : 进行切割
				tm_list = tm.split(':')
				minute = int(tm_list[0]) * 60
				second = float(tm_list[1])
				# 将总的时间计算出来
				float_time = minute + second
				# 创建一个字典
				dic_tmp = {
					'time': float_time,
					'lrc': info
				}
				# 将歌词信息存放到列表中
				self.lrc_list.append(dic_tmp)

		# print(self.lrc_list)
		# 将这个列表，按照字典中的time进行排序
		self.lrc_list.sort(key=lambda x: x['time'], reverse=1)
		print(self.lrc_list)

	# 给我一个浮点时间，给你一句歌词信息
	def time2lrc(self, float_time):
		lrc = ''
		for dic_tmp in self.lrc_list:
			if float_time >= dic_tmp['time']:
				lrc = dic_tmp['lrc']
				break
		return lrc

# lt = [
# 	{'time': 3, 'lrc': '比翼双飞'},
# 	{'time': 4, 'lrc': '说什么王权富贵'},
# 	{'time': 5, 'lrc': '怕什么戒律清规'}
# ]
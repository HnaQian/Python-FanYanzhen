class Student(object):
	def __init__(self, name, sex, age, score, number):
		super().__init__()
		self.name = name
		self.sex = sex
		self.age = age
		self.score = score
		self.number = number

	def __str__(self):
		return '%s,性别%s,年龄%s,分数为%s,学号为%s' % (self.name, self.sex, self.age, self.score, self.number)

class BanJi(object):
	def __init__(self, number, slogan, student_list=[]):
		self.number = number
		self.slogan = slogan
		# 用来管理所有的学生, 列表中存放的应该都以一个一个的学生对象
		self.student_list = student_list

	def show(self):
		# 遍历列表
		for student in self.student_list:
			print(student)

	# 根据学号查找学生
	def find(self, number):
		for stu in self.student_list:
			if stu.number == number:
				return stu
		return None

	# 添加学生
	def add(self, stu):
		# self.student_list.append(stu)
		# 根据学号判断有没有这个学生，有就不添加，没有才添加
		for stu_tmp in self.student_list:
			if stu_tmp.number == stu.number:
				print('学生已经存在')
				break
		else:
			# 添加学生
			self.student_list.append(stu)
			print('学生添加成功')

	# 根据学号删除学生
	def delete(self, number):
		for stu in self.student_list:
			if stu.number == number:
				self.student_list.remove(stu)
				print('删除成功')
				return 
		print('学生不存在，无法删除')

	def paixu(self):
		# 学生按照分数逆序排列
		# self.student_list.sort(key=lambda x: x.score, reverse=1)

		# 学生按照学号升序排列
		self.student_list.sort(key=lambda x: x.number, reverse=0)
		
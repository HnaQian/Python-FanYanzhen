from guanfang_func import Demo
from guishudi import GuiShu
from yinghan import YingHan
from geci import LrcManager
import time
import os
from banji import Student
from banji import BanJi
import random

def main():
	# d = Demo()
	# print(d.int2string(4598))
	# print(type(d.int2string(4598)))

	# print(list('1234'))
	# print(Demo.string2int('1234'))
	# print(type(Demo.string2int('1234')))

	# print(d.string2float('3.14'))
	# print(type(d.string2float('3.14')))

	# print(d.my_rfind('i lovea you lovea me', 'loveb'))
	# print(d.fenge('IamAgoodBOY'))

	# print(d.lala('aA123BbcdeD'))

	# print(d.inorin('boy', 'bell love 18 years girl'))

	# path = r'C:\Users\ZBLi\Desktop\1803\ziliao\phone.txt'
	# g = GuiShu(path)
	# print(g.phone2area('136'))

	# zidian = YingHan()
	# print(zidian.english2chinese('class'))

	# path = r'C:\Users\ZBLi\Desktop\1803\day15\girl.txt'
	# manager = LrcManager(path)
	# i = 0
	# while 1:
	# 	geci = manager.time2lrc(i)
	# 	os.system('cls')
	# 	print(geci)
	# 	i += 0.5
	# 	time.sleep(0.5)

	# names = ['鹿晗', '吴亦凡', '白敬亭', '李易峰', '张艺兴']
	# ages = [27, 28, 20, 30, 26]
	# numbers = ['1001', '1010', '1009', '1065', '1083']
	# lt = []
	# for name in names:
	# 	age = ages[names.index(name)]
	# 	score = random.randint(0, 100)
	# 	number = numbers[names.index(name)]
	# 	stu = Student(name, '男', age, score, number)
	# 	lt.append(stu)

	ming = Student('黄晓明', '男', 39, 65, '1010')
	fen = Student('贝多芬', '男', 203, 0, '1006')
	li = Student('张国立', '男', 60, 90, '1002')
	wang = Student('王宝强', '男', 33, 1, '1009')
	lt = [ming, fen, li, wang]
	print(lt)
	banji = BanJi('python1803', '好好学习天天向上', lt)
	# print(banji.student_list[0].name)
	# banji.show()
	# print(banji.find('1011'))

	du = Student('大都督', '男', 40, 1, '1008')
	banji.add(du)
	# banji.show()

	# banji.delete('1008')
	# banji.show()

	banji.show()
	banji.paixu()
	print('*' * 50)
	banji.show()



	# 创建班级对象
	# banji = BanJi('python1803', '好好学习天天向上')
	# banji.show()
	# print(banji.find('1011'))


if __name__ == '__main__':
	main()
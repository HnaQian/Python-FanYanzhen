from functools import reduce
class Demo(object):
	# str函数的实现
	# 1234  ===>  '1234'
	def int2string(self, number):
		# 先将所有数字提取出来，存放到列表中
		lt = []
		# 取余求商的原理
		while number:
			number, yushu = divmod(number, 10)
			# 将余数保存到列表中
			lt.append(yushu)
		# 将lt逆序一把
		lt.reverse()
		# [1, 2, 3, 4]  ==>  ['1', '2', '3', '4']
		# 将数字1转化为字符1
		ret = list(map(lambda x: chr(x + 48), lt))
		# ret ==> ['1', '2', '3', '4']
		return ''.join(ret)

	# 字符串转化为整型
	def string2int(self, string):
		# 先将字符串中的每个字符串存放到列表中
		# ['1', '2', '3', '4']
		lt = list(string)
		# 将上面的列表转化为 [1, 2, 3, 4]
		ret = list(map(lambda x: ord(x) - 48, lt))
		# 将[1, 2, 3, 4] ==> 1234
		number = reduce(lambda x, y: x*10 + y, ret)
		return number

	# 将浮点字符串转化为浮点
	def string2float(self, string):
		# 首先将字符串按照点切割
		lt = string.split('.')
		# 取出整数位
		zheng = self.string2int(lt[0])
		# 取出小数  14
		xiao = lt[1]
		count = len(xiao)
		# 得到小数
		xiaoshu = self.string2int(xiao) * pow(10, -count)
		return zheng + xiaoshu

	# 从string1中找到string2第一次出现的位置，没有返回-1
	# abcdefabc  cbafedcba
	# ab   6     ba   1
	def my_rfind(self, string1, string2):
		index = -1
		for i in range(len(string1) - len(string2) + 1):
			# 字符串提取
			string = string1[i:i + len(string2)]
			if string == string2:
				index = i
				# break
		return index

	# 分割字符串
	def fenge(self, string):
		ret = ''
		# 因为循环中要用i + 1，所以循环到最后必须是len - 1
		for i in range(len(string) - 1):
			ret += string[i]
			if (string[i].isupper() and string[i + 1].islower()) or (string[i].islower() and string[i + 1].isupper()):
				ret += ' '
		# 将最后一个添加过来
		ret += string[-1]
		# print(ret)
		return ret.capitalize()

	# 排序
	def abs_paixu(self, lt):
		return lt.sort(key=abs)

	# 字符串排序
	def lala(self, string):
		# 首先将数字字符干掉，将所有的小写字母放到列表中，将所有的大写字母放到列表中
		lt_lower = []
		lt_upper = []
		for x in string:
			if x.islower():
				lt_lower.append(x)
			elif x.isupper():
				lt_upper.append(x)
		# print(lt_lower)
		# print(lt_upper)
		# 将所有的小写字母排序
		lt_lower.sort()
		ret = ''
		# 生成新的字符串
		for x in lt_lower:
			# 在添加x之前，需要判断，将x转化为大写之后，这个大写字母在不在lt_upper中，如果在，需要先加大写，再加x，如果不在，直接添加x
			y = x.upper()
			if y in lt_upper:
				ret += y
			ret += x
		return ret

	# 判断string1的所有的字符是否都在string2里面，都在返回True，有一个不在返回False
	def inorin(self, string1, string2):
		flag = 0
		for x in string1:
			if x not in string2:
				flag = 1
				# return False
		# else:
		# return True
		if flag == 0:
			return True
		else:
			return False

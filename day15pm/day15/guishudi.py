class GuiShu(object):
	"""docstring for GuiShu"""
	# 方法：给手机号，返回归属地
	# 属性：
	def __init__(self, path):
		super().__init__()
		self.path = path
		self.dic = {}
		# 解析文件
		self.parse()

	# 解析文件函数
	def parse(self):
		fp = open(self.path, 'r', encoding='utf8')
		lines_list = fp.readlines()
		fp.close()

		# 遍历lt，然后取出指定的键值
		for line in lines_list:
			# 将line按照 | 进行切割
			lt = line.split('|')
			phone = lt[1]
			area = lt[-1]
			# 将其字典中
			self.dic[phone] = area

	# 这是给外部提供的接口
	def phone2area(self, phone):
		count = len(phone)
		if count < 7:
			print('手机号长度不够')
			return
		elif count > 7:
			phone = phone[:7]

		if phone in self.dic:
			return self.dic[phone]
		else:
			return None
		
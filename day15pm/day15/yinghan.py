class YingHan(object):
	def __init__(self):
		path = r'C:\Users\ZBLi\Desktop\1803\ziliao\dict_eng.txt'
		# 解析完毕之后，将内容保存到这个字典中
		self.dic = {}
		self.parse(path)

	# 解析文件，保存到字典中
	def parse(self, path):
		fp = open(path, 'r', encoding='utf8')
		# 将内容全部读取进来
		string = fp.read()
		fp.close()
		# 将内容安装 # 进行切割
		lt = string.split('#')
		# print(lt)
		# 遍历lt，处理lt中每一个数据
		for x in lt:
			if x == '':
				continue
			# 'zoonosis\nTrans:n. 动物病\n'
			# 将x用 \nTrans: 进行切割
			ret = x.split('\nTrans:')
			english = ret[0]
			chinese = ret[1].rstrip('\n')
			chinese = chinese.replace('@', '\n')
			# 将英文作为键，中文作为值保存到字典中
			self.dic[english] = chinese

	# 给一个英文，返回一个中文解释
	def english2chinese(self, english):
		# if english in self.dic:
		# 	return self.dic[english]
		# else:
		# 	return None

		return self.dic.get(english)
string = '秦时明月汉时关,万里长征人未还,但使龙城飞将在,不教胡马度阴山'
# 石达开  曾国藩
# 大名王朝1566
# for x in string:
# 	print(x)

# 鹈鹕
# lt = ['隆多', '戴维斯', '霍乐迪', '考辛斯']
# for x in lt:
# 	print(x)

# dic1 = {'鹈鹕队': '隆多', '火箭队': '哈登', '勇士队': '库里', '马刺队': '波波维奇'}
# for x in dic1:
# 	print('键是%s, 值是%s' % (x, dic1[x]))
# 直接遍历键值
# for k, v in dic1.items():
# 	print('键是%s, 值是%s' % (k, v))

# for x in range(1, 101):
# 	print(x)
# for x in range(10):
# 	print(x)

# for x in range(1, 10, 3):
# 	print(x)

# for x in range(2, 101, 2):
# 	print(x)

# for x in range(10, 1, -2):
# 	print(x)

# 打印大写字母Z-A
# for x in range(90, 64, -1):
# 	print(chr(x))

# for x in range(1, 10):
# 	if x == 3:
# 		# continue
# 		break
# 	print(x)
# else:
# 	print('循环正常结束')

# for x in range(1, 10):
# 	print('马云真厉害')
# 	for y in range(1, 10): 
# 		print('王宝强真厉害')

# 遍历列表，列表中有好多元素，每一个元素又都是字典
lt = [
	{'name': '田馥甄', 'age': '36', 'height': '162'},
	{'name': '柳岩', 'age': '33', 'height': '170'},
	{'name': '林志玲', 'age': '42', 'height': '175'},
	{'name': '柳慧芬', 'age': '18', 'height': '163'},
]

# for x in lt:
# 	for k, v in x.items():
# 		print(k + '======>' + v, end=',')
# 	print('')

# 练习
'''
lt = [
	{'name': '田馥甄', 'age': '36', 'info': [('phone', '1383838438'), ('address', '北京')]},
	{'name': '柳岩', 'age': '33', 'info': [('phone', '139809808'), ('address', '河南郑州')]},
	{'name': '林志玲', 'age': '42', 'info': [('phone', '13767655465'), ('address', '河北石家庄')]},
	{'name': '柳慧芬', 'age': '18', 'info': [('phone', '13737623'), ('address', '山东济南')]},
]
打印：我叫xxx，我的手机号为xxx，我来自xxx
'''

lt = [
	{'name': '田馥甄', 'age': '36', 'info': [('phone', '1383838438'), ('address', '北京')]},
	{'name': '柳岩', 'age': '33', 'info': [('phone', '139809808'), ('address', '河南郑州')]},
	{'name': '林志玲', 'age': '42', 'info': [('phone', '13767655465'), ('address', '河北石家庄')]},
	{'name': '柳慧芬', 'age': '18', 'info': [('phone', '13737623'), ('address', '山东济南')]},
]

# for x in lt:
# 	name = x['name']
# 	info = x['info']
# 	for k, v in info:
# 		if k == 'phone':
# 			phone = v
# 		else:
# 			address = v
# 	print('我叫%s,我的手机号为%s,我来自%s' % (name, phone, address))

# for x in lt:
	# name = x['name']
	# info = x['info']
	# 将[('',''),('','')]此种形式的列表作为字典处理，遍历列表
	# for k, v in info:
	# 	if k == 'phone':
	# 		phone = v
	# 	else:
	# 		adress = v
	# print('我叫%s,我的手机号为%s,我来自%s' % (name, phone, address))






# for x in lt:
# 	name = x['name']
# 	phone = x['info'][0][1]
# 	address = x['info'][1][1]
# 	print('我叫%s,我的手机号为%s,我来自%s' % (name, phone, address))

# dic2 = {'name': 'goudan', 'age': 18}
# print(dic2.items())
'''
'a' 'b'  97-122
1、打印97-122
2、将数字转化为字母
'''
# i = 97
# while i <= 122:
# 	print(chr(i))
# 	i += 1

# i = 122
# while i >= 97:
# 	print(chr(i))
# 	i -= 1

# c = input('请输入一个字母:')
# if (c >= 'A') and (c <= 'Z'):
# 	c = chr(ord(c) + 32)
# else:
# 	c = chr(ord(c) - 32)
# print(c)
# c = input('请输入一个字母：')
# if (c >= 'A') and (c <= 'Z'):
# 	c = chr(ord(c) + 32)
# else:
# 	c = chr(ord(c) - 32)
# print(c)

# c = input('请输入一个字母：')
# if (c >= 'A') and (c <= 'Z'):
# 	c = chr(ord(c) + 32)
# else:
# 	c = chr(ord(c) - 32)
# print(c)



# a = int(input('请输入a:'))
# b = int(input('请输入b:'))
# c = int(input('请输入c:'))
# print(a, b, c)
# print('*' * 10)
# if a < b:
# 	a, b = b, a
# if a < c:
# 	a, c = c, a
# if b < c:
# 	b, c = c, b
# print(a, b, c)
# print(c, b, a)
# 还有一种比较方式，自己实现
# 从大到小排列
# a = int(input('请输入a:'))
# b = int(input('请输入b:'))
# c = int(input('请输入c:'))
# print(a, b, c)
# print('*' * 10)
# if a < b:
# 	a, b = b, a
# if a < c:
# 	a, c = c, a
# if b < c:
# 	b, c = c, b
# print(a, b, c)
# 从大到小排列

# a = int(input('请输入a:'))
# b = int(input('请输入b:'))
# c = int(input('请输入c:'))
# print(a, b, c)
# print('*' * 10)
# if a < b:
# 	a, b = b, a
# if b < c:
# 	b, c = c, b
# if a < b:
# 	a, b = b, a
# if a < b:
# 	a, b = b, a
# if a < c:
# 	a, c = c, a
# if b < c:
# 	b, c = c, b

# print(a, b, c)
# if a > b:
# 	a, b = b, a
# if b > c:
# 	b, c = c, b
# if a > b:
# 	a, b = b, a
# print(a, b, c)
# if a > b:
# 	a, b = b, a
# if b > c:
# 	b, c = c, b
# if a > b:
# 	a, b = b, a
# print(a, b, c)


# 将最大的数给A,第二大的数给B，最小的数给C
# 三个if并列要按着都判断一次。转变思维，变量的思维。程序自上而下运行。
# if a < b:
# 	a, b = b, a
# if a < c:
# 	a, c = c, a
# if b < c:
# 	b, c = c, b
# print(a, b, c)







'''
username = 'goudan'
password = '123456'

input_name = input('请输入用户名:')

# 计数器
count = 0

if input_name == username:
	# 用户名正确，提示输入密码
	while count < 3:
		input_pwd = input('请输入密码:')
		# count加1
		count += 1
		if input_pwd == password:
			print('登录成功')
			break
		else:
			print('密码不正确')
			# if count == 3:
			# 	print('次数过多，请5分钟之后在输入')
	else:
		print('次数过多，请5分钟之后在输入')
else:
	print('用户名不正确')
'''
# username = 'goudan'
# password = 123456

# count = 0
# input_name = input('请输入用户名：')
# if input_name == username:
# 	while count < 3:
# 		count += 1
# 		input_pwd = int(input('请输入密码：'))
# 		if input_pwd == password:
# 			print('登录成功')
# 			break
# 		elif count == 3:
# 			print('请3分钟之后再次输入')
# 		else:
# 			print('密码错误，请重新输入：')

# else:
# 	print('用户名不正确')
username = 'goudan'
password = 12345

count = 0
input_name = input('请输入用户名：')
if input_name == username:
	while count < 3:
		input_pwd = int(input('请输入密码：'))
		if input_pwd == password:
			print('登录成功')
			break
		else:
			print('密码错误')
			count += 1
			if count == 3:
				print('次数较多，1分钟后再输入 ')
else:
	print('用户名错误')








'''
import time
hour = int(input('请输入小时:'))
minute = int(input('请输入分钟:'))
second = int(input('请输入秒:'))

while 1:
	second += 1
	# 判断second是不是60，如果是60，要往上进位
	if second == 60:
		second = 0
		minute += 1
		if minute == 60:
			minute = 0
			hour += 1
			if hour == 24:
				hour = 0

	print('%02d:%02d:%02d' % (hour, minute, second))
	time.sleep(1)
# '''

# string = 'abcde123244A,JNDF   hello&$#$kajdfkja12343'
# i = 0
# count_word = 0
# count_space = 0
# count_number = 0
# while i < len(string):
# 	if ((string[i] >= 'a') and (string[i] <= 'z')) or ((string[i] >= 'A') and (string[i] <= 'Z')):
# 		count_word += 1
# 	elif string[i] == ' ':
# 		count_space += 1
# 	elif (string[i] >= '0') and (string[i] <= '9'):
# 		count_number += 1
# 	i += 1
# print('字母的个数为%s,空格的个数为%s,数字的个数为%s' % (count_word, count_space, count_number))
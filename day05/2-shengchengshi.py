# lt = [1, 2, 3, 4, 5, 6, 7, 8, 9]
lt = list(range(1, 10))
# lt = range(1, 5)
# print(type(lt))

# 列表生成式
lt = [x for x in range(1, 10)]
lt = [2*x for x in range(1, 10)]
lt = [x*x for x in range(1, 10)]
lt = [str(x) for x in range(1, 10)]
lt = [x for x in range(1, 10) if x % 2 == 0]
# print(lt)

# 字典生成式
dic1 = {'name': 'goudan', 'age': '20'}
lt = [('t-mac', '1'), ('wade', '3')]
# dic2 = {v: k for k, v in dic1.items()}
dic3 = {v: k for k, v in lt}

# print(dic3)

print('李白')
print('杜甫')
print('白居易')
exit()
print('李商隐')
print('杜牧')
print('武则天')
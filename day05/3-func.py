# dream()

# 函数的格式
def dream():
	print('桂林山水甲天下')
	print('阳朔风景甲桂林')
	# baby()

# dream()

def baby():
	print('我叫你叫baby，你说我流氓')

# def dream():
# 	print('我想去拉萨')

# dream()
# 函数调用
# dream()



# print('少林寺')

def kitty(name, agelala, height):
	print('我叫%s, 我的年龄为%s, 我的身高为%s' % (name, agelala, height))

# 调用函数,就要传递参数，传参
name1 = '唐太宗'
age1 = '61'
height1 = '165'
# kitty(name1, age1, height1)
# kitty(age1, height1, name1)
# kitty(height=height1, name=name1, agelala=age1)


def hello():
	my_sum = 0
	for x in range(1, 101):
		my_sum += x
	# print(my_sum)
	return my_sum, 'hello', 'baby'
	# return 'hello'

# a = hello()
# print(a)
# print(hello())

# 写一个函数，传递两个参数，返回两个参数的和
def add(a, b):
	return a + b
	# print(a + b)

# c = add(5, 5)
# print(c)
# add(5, 5)

def my_sum(n):
	ret = 0
	for x in range(1, n + 1):
		ret += x
	return ret

a = my_sum(100)
# print(my_sum(100))
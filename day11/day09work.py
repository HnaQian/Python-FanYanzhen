import os
import time

# 单元测试   黑盒测试   测试用例
# 第一题  os.path.splitext
def my_split_ext(path):
	'''
	找到最后一个点，然后安装最后一个点进行切割
	'''
	index = path.rfind('.')
	# 找到前面的一部分
	first = path[:index]
	# 找到后面的部分
	second = path[index:]
	return first, second

# 第二题 os.path.join
def my_join(dirpath, filename):
	dirpath = dirpath.rstrip('/') + '/'
	return dirpath + filename

# 第三题
def my_copy(srcpath, dstpath):
	# 判断源文件存在不存在, 入口参数检测
	if not os.path.exists(srcpath):
		print('哥们，源文件不存在，如何拷贝')
		return
	if not os.path.isfile(srcpath):
		print('哥们，是文件夹，不能拷贝')
		return
	if os.path.isfile(dstpath):
		print('目标文件存在，不能拷贝')
		return
	# 打开这两个文件，以只读方式打开srcpath，以只写方式打开dstpath
	fp_r = open(srcpath, 'rb')
	fp_w = open(dstpath, 'wb')
	# 每次读100个字节，拷贝过去
	# 获取文件的大小
	size = os.path.getsize(srcpath)
	number = 1024
	while 1:
		# 每次读取100个字节
		content = fp_r.read(number)
		fp_w.write(content)
		# 读完100个字节，将size进行改变
		size -= number
		# 判断循环退出的条件
		if size <= 0:
			break
		# print('正在拷贝-----')
		# time.sleep(0.3)

	# 关闭文件
	fp_r.close()
	fp_w.close()

# 第四题，批量修改文件名字
def piliang(path, prefix):
	# 自己添加对文件夹的判断
	# 列出文件夹下所有的文件
	filename_list = os.listdir(path)
	# 遍历这个列表，将名字进行修改
	for filename in filename_list:
		# 拼接得到文件的旧全路径
		oldpath = os.path.join(path, filename)
		# 先得到文件的新的名字
		newname = prefix + filename
		# 得到文件的新全路径
		newpath = os.path.join(path, newname)
		# print(oldpath)
		# 修改名字
		os.rename(oldpath, newpath)

# 第五题：递归删除文件夹
# 第六题：自己提供
def digui_dir(path):
	# 思路：遍历这个文件夹，判断如果是文件，删除文件，如果是文件夹，递归调用自己
	filename_list = os.listdir(path)
	for filename in filename_list:
		# 拼接得到文件的路径
		filepath = os.path.join(path, filename)
		# 判断filepath是文件还是文件夹
		if os.path.isfile(filepath):
			# 直接删除文件
			os.remove(filepath)
		else:
			# 递归调用自己
			digui_dir(filepath)
	# 删除最外层的空文件夹
	os.rmdir(path)

# 第七题
def youbian(bian):
	path = r'C:\Users\ZBLi\Desktop\1803\day09/youbian.txt'
	fp = open(path, 'r', encoding='utf8')
	line_list = fp.readlines()
	fp.close()
	# 定义一个空字典
	dic = {}
	# print(line_list)
	# 遍历line_list  将里面每一个列表字符串转化为列表
	for line in line_list:
		# 将字符串后面的无用字符删除掉
		line = line.rstrip(',\n')
		# 将line字符串转化为一个列表
		lt = eval(line)
		dic[str(lt[0])] = lt[1]

	# print(dic)
	if bian in dic:
		return dic[bian]
	else:
		return '没有这个遍'


# 开房查询   刚  芈  操
def kaifang(gemen='种亚男'):
	path = r'C:\Users\ZBLi\Desktop\1803\day09/kaifanglist.txt'
	fp = open(path, 'r', encoding='utf8')
	line_list = fp.readlines()
	fp.close()

	# print(line_list)
	info_list = []
	for line in line_list:
		# 如果是换行，直接略过
		if line == '\n':
			continue
		lt = line.split(',')
		if lt[0] == gemen:
			info_list.append(line)

	if len(info_list) == 0:
		# 没有找到
		print('他是一个好男人')
		return
	else:
		filename = gemen + '.txt'
		# 找到了
		fp_w = open(filename, 'w', encoding='utf8')
		# 如何将列表写入到文件中
		fp_w.write(str(info_list))
		fp_w.close()



def main():
	# path = r'C:\Users\ZBLi\Desktop\1803\day11/day11.txt.mp3.avi'
	# print(os.path.splitext(path))
	# print(my_split_ext(path))

	# dirpath = r'C:\Users\ZBLi\Desktop\1803\day11'
	# filename = 'day11.txt'
	# print(os.path.join(dirpath, filename))
	# print(my_join(dirpath, filename))

	# srcpath = r'C:\Users\ZBLi\Desktop\1803\video/7-3调试和递归.wmv'
	# dstpath = r'C:\Users\ZBLi\Desktop\1803\day11/lala.wmv'

	# my_copy(srcpath, dstpath)

	# path = r'C:\Users\ZBLi\Desktop\1803\day11\lala'
	# piliang(path, '千锋-')

	# path = r'C:\Users\ZBLi\Desktop\1803\day11/lala'
	# digui_dir(path)

	# bian = '110009'
	# print(youbian(bian))

	kaifang('张飞')

if __name__ == '__main__':
	main()
# 字符串查找功能
# lovebaby  8
# baby  4
# love oveb veba ebab baby
def my_find(string1, string2):
	# 遍历string1
	for i in range(len(string1) - len(string2) + 1):
		string = string1[i:i + len(string2)]
		if string == string2:
			return i
	return -1

# 第七题
# 0 1 1 2
def feibo(n):
	lt = []
	a = 0
	b = 1
	for x in range(n):
		lt.append(b)
		a, b = b, a + b
	return lt

# 第8题，字符串去重
def string_chong(string):
	# 定义一个新的字符串
	ret = ''
	for x in string:
		if not x in ret:
			ret += x
	return ret

# 第9题
def kongge(phone_string):
	# 取出前3个
	'''
	a = phone_string[0:3]
	b = phone_string[3:7]
	c = phone_string[7:11]
	return a + ' ' + b + ' ' + c
	'''

	# 先将字符串逆序
	phone_string = phone_string[::-1]
	ret = ''
	for i in range(len(phone_string)):
		# 判断如果i是4的倍数，加空格
		if i % 4 == 0:
			ret += ' '
		ret += phone_string[i]
	return ret[::-1]

import random
def shuangse():
	# 随机取样
	ret = [x for x in range(1, 34)]
	ret = random.sample(ret, 6)
	ret.append(random.randint(1, 17))
	# 可以随机打乱
	return ret

# 解压缩  'a3b2c1d2'
def jieyasuo(string):
	# 遍历字符串下标
	ret = ''
	for i in range(len(string)):
		if string[i].isdecimal():
			count = int(string[i])
			lala = string[i - 1] * count
			ret += lala
	return ret

def main():
	# print(my_find('lovebaby', 'baby'))

	# print(feibo(5))
	# print(string_chong('ddabcabb'))

	# print(kongge('13338384381'))
	# print(shuangse())
	print(jieyasuo('a3b2c1d2'))

if __name__ == '__main__':
	main()
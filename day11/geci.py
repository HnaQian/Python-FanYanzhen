import time
import os
# 将歌词信息解析为一个字典
# 时间是键，歌词是值
def parse(line_list):
	# 定义一个空字典
	dic = {}
	# 遍历这个列表
	for line in line_list:
		# 只解析歌词信息
		if (line == '\n') or (not line.startswith('[0')):
			continue
		# 解析所有的歌词信息
		# [02:11.55][01:50.60][00:22.63]穿过幽暗的岁月
		# 将字符串切割  ] 
		# [02:11.55 [01:50.60 [00:22.63 穿过幽暗的岁月
		line = line.rstrip('\n')
		lt = line.split(']')
		# 取出歌词信息
		geci = lt[-1]
		# 获取时间列表
		time_list = lt[:-1]
		# 遍历时间列表，得到每一个浮点时间
		for time_info in time_list:
			# [02:11.55
			time_info = time_info.lstrip('[')
			# 02:11.55
			# 将字符串按照 : 进行切割
			time_info_list = time_info.split(':')
			# 02 11.55
			float_time = int(time_info_list[0]) * 60 + float(time_info_list[-1])
			# 将时间作为键，歌词作为值，存放到字典中
			dic[float_time] = geci
	return dic

# 给我一个时间，给你一句歌词
def time_geci(dic, float_time):
	# 给一个时间，返回指定的歌词
	# 取出来字典中所有的键
	keys = list(dic)
	# 将时间进行排序
	keys.sort()
	# print(keys)
	# 遍历这个列表，找到列表中第一个比所给时间大的元素的前一个元素即可
	if keys[0] > float_time:
		geci = dic[keys[0]]
		return geci
	for i in range(len(keys)):
		if keys[i] > float_time:
			geci = dic[keys[i - 1]]
			# 结束循环
			return geci 
	else:
		# 如果时间大于最后一句歌词的时间，歌词显示最后一句的歌词信息
		geci = dic[keys[i]]
		return geci

def main():
	# 歌词写到文件中，从文件中读取
	fp = open('geci.txt', 'r', encoding='utf8')
	# 按行读取歌词
	line_list = fp.readlines()
	fp.close()
	# 解析歌词
	dic = parse(line_list)
	print(dic)
	# float_time = 0
	# while 1:
	# 	geci = time_geci(dic, float_time)
	# 	os.system('cls')
	# 	print(geci)
	# 	float_time += 0.3
	# 	time.sleep(0.3)

if __name__ == '__main__':
	main()
class Person(object):
	"""docstring for Person"""
	def __init__(self, name, age, height):
		# 依次的将成员属性进行初始化
		self.name = name
		self.age = age
		self.height = height


	def kancai(self):
		print('我叫%s, 我年龄为%s, 我身高为%s, 我喜欢上山砍材' % (self.name, self.age, self.height))

	def swim(self):
		print('我叫%s, 我年龄为%s, 我身高为%s, 我喜欢下河游泳' % (self.name, self.age, self.height))

	def fish(self):
		print('我叫%s, 我年龄为%s, 我身高为%s, 我喜欢隔壁吃鱼' % (self.name, self.age, self.height))
# 两个类
# 一个是Person类
class Person(object):
	def __init__(self, name, age, height, car):
		self.name = name
		self.age = age
		self.height = height
		self.car = car

	# 社交方法
	def social(self):
		print('美女们请注意, %s要出没，大家回家锁门不要出来' % self.name)
		self.car.run()
		# 创建一辆车  
		# 这么写代码不好，因为这辆车就写死了，无论谁去社交，开的都是这辆车
		# car = Car('兰博基尼', '骚粉', '800')
		# print('老王准备开车')
		# car.run()
# 一个是Car类
class Car(object):
	def __init__(self, logo, color, price):
		self.logo = logo
		self.color = color
		self.price = price

	def run(self):
		print('%s%s汽车在奔跑,价格为%s万RMB' % (self.logo, self.color, self.price))

car = Car('别克君威', '白色', '23')
car1 = Car('小牛电动车', '黑色', '0.6')
car2 = '这是一辆车'
wang = Person('王大锤', 35, 177, car1)
wang.social()

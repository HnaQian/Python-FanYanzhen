class Dog(object):
	# name = '旺财'
	def __init__(self, new_name, new_age):
		# print('在创建对象的时候会被调用')
		self.name = new_name
		self.age = new_age
		# self.lt = []
		# self.dic = {}
		self.wangwang()

	def say(self):
		print('我叫%s, 我的狗龄为%s' % (self.name, self.age))
		# 在这让狗叫一声
		self.wangwang()
		self.swim(100)

	def wangwang(self):
		# 在其它的成员方法中，也可以动态的添加属性，但是一般不这么做，因为这么写不清晰
		# self.height = 80
		print('%s在汪汪' % self.name)

	def swim(self, number):
		print('天生会游泳,一次游%s千米' % number)

dog = Dog('大黄', 3)
# dog.say()
# dog.wangwang()
# dog1 = Dog('小黑', 2)
# dog1.say()
# dog.wangwang()
# print('名字:%s, 身高%s' % (dog.name, dog.height))
# dog.name = '旺财'
# dog.wangwang()
# print(dog.name)

dog1 = Dog('小黑', 2)
# dog1.wangwang()
# print(dog1.name)




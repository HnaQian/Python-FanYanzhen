class GirlFriend(object):
	"""我的女朋友类"""

	# 这么写，保证了不同的对象拥有相同的属性，但是他们的属性值都全部一样了，这个不符合要求
	# name = '小芳'
	# age = 20
	# height = '180'
	# weight = '100'
	def __init__(self, height, age):
		# print('我被调用')
		# print(self)
		# pass
		self.height = height
		self.age = age
	def wash(self):
		print(self)
		print('妞，过来，把这几件衣服给洗了')

	def cook(self):
		print('给你做一个你最喜欢吃的泡面吧')
	
	def anmo(self):
		print('过来，给朕按摩按摩颈椎')

fang = GirlFriend('200', 18)
print(fang.height)
li = GirlFriend('300', 20)
print(li.height)
# print(fang)
# fang.height = '200'
# print(fang)
# print('*' * 30)
# fang.wash()
# li = GirlFriend()
# print(li)
# li.wash()
# print(fang.height)
# print(li.height)

# xiaofang = GirlFriend()
# xiaofang.wash()
# 给对象添加属性,给对象动态添加属性
# xiaofang.name = '闫晓芳'
# xiaofang.age = '16'
# xiaofang.height = '160'
# xiaofang.weight = '95'

# print(xiaofang.name)
# print(xiaofang.age)

# xiaoli = GirlFriend()
# xiaoli.name = '张小丽'
# xiaoli.age = '18'

# print(xiaoli.height)
# xiaoli.wash()
'''
人类
	属性
		姓名
		年龄
		身高
		体重
		性别
	方法
		吃饭
		睡觉
		打游戏
		洗澡
		喝酒
'''

class Person(object):
	"""有关这个类的说明信息"""
	# 写方法就是写函数，以前怎么写，现在还怎么写
	def eat(self):
		print('我喜欢吃合记烩面')

	def my_sleep(self):
		print('我以前非常能睡觉')

	def wangzhe(self):
		print('我喜欢用亚瑟')

	def drink(self, name, liang):
		# print(self)
		print('我喜欢喝%s,我能喝%s' % (name, liang))

# 通过类创建一个对象
obj = Person()
# obj的类型就是Person，Person和list、str、tuple、dict是同一级的概念，只不过list、str都是官方的类，而Person是自己定义的类
# print(type(obj))

# print(obj)
# print('*' * 30)
obj.drink('汾酒', '两斤')

# 通过对象调用方法
# a = 'hello'
# print(type(a))
# b = a.upper()
# b = str.upper(a)
# print(b)

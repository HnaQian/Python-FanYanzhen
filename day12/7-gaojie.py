lt = [1, 2, 3, 4, 5]
# lt1 = []
# for x in lt:
# 	lt1.append(x ** 2)
# print(lt1)
def pingfang(x):
	return x ** 2

# ret = map(pingfang, lt)
# ret = map(lambda x: x ** 2, lt)
# ret = list(ret)
# print(ret)

# lt = ['hello', 'baby', 'haha', 'world']
# ret = list(map(len, lt))
# print(ret)

# filter什么意思   过滤
# lt = [1, 2, 3, 4, 5, 6, 7, 8, 9]

# ret = list(filter(lambda x: x % 2 == 0, lt))
# print(ret)

# lt = ['', 'baby', 'haha', '', 'love', '', 'dudu']
# ret = list(filter(lambda x: bool(x), lt))
# print(ret)


# reduce 学习
from functools import reduce
lt = [1, 2, 3, 4, 5, 6, 7]
lt1 = [1, 3, 9, 8]
# ret = reduce(lambda x, y: x + y, lt)
# ret = reduce(lambda x, y: x*10 + y, lt1)
# print(ret)

# sorted  sort
lt = [100, 89, 99, 39, 67]

# 直接将当前列表进行排序
# lt.sort()
# print(lt)

# 给我一个列表，将排序好的列表返回给你，原列表不变
lt1 = sorted(lt, reverse=1, key=lambda x: x['age'])
print(lt)
print(lt1)
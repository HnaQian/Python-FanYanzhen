class Cat(object):
	def __init__(self, name, age, height):
		self.name = name
		self.age = age
		self.height = height

	def miao(self):
		print('喵喵喵')

	def __str__(self):
		# print('我被调用了')
		return '我叫%s,我年龄为%s,我身高为%s' % (self.name, self.age, self.height)

hua = Cat('小花', 4, 20)
h = Cat('hehe', 3, 18)
print(hua)
print(h)
'''
def test(a, 
	return a + b

# print(test())

# 默认值
# def hello(name, string='哥曾经也是小草级别的'):
	# print(name + string)
# def hello(string='haha'):
	# print(string)

# hello('你是谁')
# print(hello())
# hello('hehe', 'haha')   


# def baby(face, leg, eye, skin):
# 	print('脸蛋%s, 腿%s, 眼睛%s, 皮肤%s' % (face, leg, eye, skin))

# # baby('耐看', '修长', '栓眼皮', '白皙')
# lt = ['漂亮', '有力', '火眼金睛', '粗糙']
# dic1 = {
# 	'face': '恶心',
# 	'leg': '粗壮',
# 	'eye': '有坑',
	'skin': '体毛旺盛',
}
# 拆包
baby(*lt)
# baby(**dic1)

def baby(**args):
	print(args)

# baby(100, 200, '李白', 'dufu', '白居易', '李时珍')
# baby(a=100, b=200, c='李白', d='dufu', e='白居易', f='李时珍')

'''


# b = '凤凰台上凤凰游，凤去台空江自流，吴宫花草埋幽径，晋代衣冠成古丘'


# 当全局变量是列表或者字典的时候，可以在函数体内直接修改

# lt = ['李清照', '李煜']
# dic1 = {'平凡的世界': '路遥', '康熙大帝': '二月河'}
# def haha():
# 	a = '沉舟侧畔千帆过，病树前头万木春'
# 	# lt[0] = '毛泽东'
# 	# print(lt)
# # 	# 定义了一个局部变量
# # 	# b = 100
# 	# dic1['穆斯林的葬礼'] = 'xuanzexi'
# # 	dic1['康熙大帝'] = '莫言' 
# # 	print(dic1)

# # haha()
# # print(dic1)
# # print(b)
# # b = '凤凰台上凤凰游,凤去台空江自流,吴宫花草埋幽径,晋代衣冠成古丘'
# b = 100
# # print(b)
# lt = ['李清照', '李煜', '辛弃疾', '苏轼']
# # dic1 = {'平凡的世界': '路遥', '康熙大帝': '二月河'}
# def haha():
# 	a = '沉舟侧畔千帆过,病树前头万木春'
# 	print(a)
# 	# print(b)
# 	# global b
# 	# b = 90
# 	# print(b)
# 	lt[0] = '毛泽东'
# 	print(lt)
# 	# dic1['穆斯林的葬礼'] = '李志斌'

# haha()
# print(dic1)
# print(b)
# print(a)



# a = 100
# # lt = ['高老庄', '高圆圆']
# def hello(name, age):
# 	a = '乾隆'
# 	print(name, age)
# 	lt[0] = '高杰伦'
# # 	global lt
# 	# lt = 100
# 	print(lt)

# # name = '爱新觉罗姚明'
# # age = '40'
# hello()
# print(lt)
# print(a)


# def helloha():
# 	print('这是hello函数')
# 	a = '奶茶妹妹'
# 	def babyha():
# 		print('这是baby函数')
# 		# nonlocal a
# 		a = '强东哥真强'
# 		print(a)
# 	babyha()
# 	print(a)

# helloha()
# babyha()

# def hello

# def hello(nameme, agege):
# 	print(nameme, agege)
# name1 = '爱新觉罗姚明'
# age1 = '40'
# hello(nameme=name1, agege=age1)
# def helloha():
# 	print('这是hello函数')
# 	a = '奶茶妹妹'
# 	def babyha():
# 		print('这是baby函数')
# 		nonlocal a
# 		a = '强东哥真强'
# 		print(a)
# 	babyha()
# 	print(a)

# helloha()


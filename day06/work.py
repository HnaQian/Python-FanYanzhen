import time
# 打印99乘法表
'''
行：9行
列：和第几行有关
1*1=1
1*2=2 2*2=4
1*3=3 2*3=6 3*3=9
1*4=4 2*4=8 3*4=12
'''
def biao99():
	for x in range(1, 10):
		# x就是行号
		for y in range(1, x + 1):
			# 每一列打印什么
			# 第y列，第x行应该打印的内容为
			print('%d*%d=%d\t' % (y, x, y * x), end='')
		print('')

# biao99()

# 第二题
def biaoge(m, n):
	# m就是几行，n就是几列
	for x in range(1, m + 1):
		for y in range(1, n + 1):
			print((x - 1)*n + y, end='\t')
		print('')

def biaoge2(m, n):
	# 计数器
	count = 0
	for x in range(1, m + 1):
		for y in range(1, n + 1):
			count += 1
			print(count, end='\t')
		print('')

# biaoge(5, 4)
# print('*' * 20)
# biaoge2(6, 9)

# 第三题
def my_max(a, b, c):
	if a < b:
		a, b = b, a
	if a < c:
		a, c = c, a
	return a

# print(my_max(10, 90, 60))

# 第四题
def paixu(a, b, c):
	if a < b:
		a, b = b, a
	if a < c:
		a, c = c, a
	if b < c:
		b, c = c, b
	# 将abc从大到小依次返回
	return a, b, c

# print(paixu(100, 10, 89))

# 第五题
def biao():
	hour = int(input('请输入小时:'))
	minute = int(input('请输入分钟:'))
	second = int(input('请输入秒:'))

	while 1:
		second += 1
		# 判断second是不是60，如果是60，要往上进位
		if second == 60:
			second = 0
			minute += 1
			if minute == 60:
				minute = 0
				hour += 1
				if hour == 24:
					hour = 0

		print('%02d:%02d:%02d' % (hour, minute, second))
		time.sleep(1)

# biao()

# 第六题
'''
10 0
5  1
2  0
1  1
0
'''
def shi2two(number):
	# number = 10
	string = ''
	while number:
		# 得到余数
		yushu = number % 2
		# 将yushu保存起来
		string += str(yushu)
		# 得到商
		number = number // 2
	# print(string)
	# 将字符串逆序即可
	return string[::-1]

# print(shi2two(15))

# 第七题
def jiecheng(n):
	jie = 1
	for x in range(1, n + 1):
		jie *= x
	return jie

def jiechenghe2(n):
	my_sum = 0
	for x in range(1, n + 1):
		my_sum += jiecheng(x)
	return my_sum


def jiechenghe(n):
	my_sum = 0
	for y in range(1, n + 1):
		# 每一次求阶乘，都应该从1开始
		jie = 1
		for x in range(1, y + 1):
			jie *= x
		my_sum += jie
	return my_sum

# print(jiechenghe2(3))

# 第八题  1234   4
def weishu(number):
	count = 0
	while number:
		number //= 10
		count += 1
	return count

# print(weishu(1234))


'''
F
FF
FFF
FFFF
FFFFF
FFFFFF
'''
def tuxing1(c):
	# 先得到要打印多少行
	line = ord(c) - ord('A') + 1
	# 循环
	for x in range(1, line + 1):
		for y in range(1, x + 1):
			print(c, end='')
		print('')
'''
F
FE
FED
FEDC
FEDCB
FEDCBA
'''
def tuxing2(c):
	# 先得到要打印多少行
	line = ord(c) - ord('A') + 1
	# 循环
	for x in range(1, line + 1):
		for y in range(1, x + 1):
			zhi = ord(c) - y + 1
			print(chr(zhi), end='')
		print('')

'''
F
EF
DEF
CDEF
BCDEF
ABCDEF
'''
def tuxing3(c):
	# 先得到要打印多少行
	line = ord(c) - ord('A') + 1
	# 循环
	for x in range(1, line + 1):
		# for y in range(1, x + 1):
		for y in range(x, 0, -1):
			zhi = ord(c) - y + 1
			print(chr(zhi), end='')
		print('')

# tuxing3('F')


# 首先理解这个，然后再去理解上面的
def lijie(c):
	# for x in range(0, 6):
	for x in range(5, -1, -1):
		zhi = ord(c) - x
		print(chr(zhi), end='')
	print('')

lijie('F')

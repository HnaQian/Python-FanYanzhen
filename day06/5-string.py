# lt = ['i', 'love', 'you', 'baby']
# string = '**'.join(lt)

# print(string)

# string = 'love'
# 推荐使用第一种方式进行调用
# s = string.ljust(50, '*')
# s = string.rjust(50, '*')
# s = string.center(50, '*')
# s = string.zfill(50)
# s = str.ljust(string, 50, '=')
# print(s)

string = '       love        '
s = string.strip()
# s = string.strip('#$&')
print(s)

# string = 'i love you love very much love, love love'
# s = string.replace('love', 'hate', 2)
# print(s)

# string = '我爱你你爱我吗我爱你姐姐你姐姐爱我吗'
# lt = string.split('爱')
# s = '狠'.join(lt)
# print(lt, s)

string = '''送元二使安西
渭城朝雨浥轻尘安西
客舍青青柳色新安西
劝君更尽一杯酒安西
西出阳关无故人安西
'''

# s = string.find('安西')
s = string.rfind('安西')

# print(s)
# lt = string.splitlines()
# lt = string.split('\n')
# print(lt)

class GirlFriend(object):
	def __init__(self, name, age, face, height, weight, skin):
		super().__init__()
		self.name = name
		self.age = age
		self.face = face
		self.height = height
		self.weight = weight
		self.skin = skin

	def song(self):
		print('经常给你送早点')

	def __str__(self):
		return '我的名字叫做%s, 年龄为%s, 脸蛋%s, 身高为%s , 体重为%s, 皮肤%s' % (self.name, self.age, self.face, self.height, self.weight, self.skin)
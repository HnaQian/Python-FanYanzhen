# 牌类
class Poker(object):
	def __init__(self, color, number):
		self.color = color
		self.number = number

	def __str__(self):
		return '%s%s' % (self.color, self.number)

# 手类
class Hand(object):
	def __init__(self, direction, poker=None):
		self.poker = poker
		# 这是左右手的属性
		self.direction = direction

	# 拿牌的方法
	def take_poker(self, poker):
		self.poker = poker

class Person(object):
	# 尔冬升    门徒（刘德华、吴彦祖、张静初、古天乐）  
	# 新宿事件  （成龙、吴彦祖、徐静蕾）
	def __init__(self, left_hand, right_hand):
		self.left_hand = left_hand
		self.right_hand = right_hand

	def catch_poker(self, poker1, poker2):
		# 让手去拿牌
		self.left_hand.take_poker(poker1)
		self.right_hand.take_poker(poker2)

	def show(self):
		print('我的左手牌为--》%s' % self.left_hand.poker)
		print('我的右手牌为--》%s' % self.right_hand.poker)

	def exchange_poker(self):
		# 交换牌
		self.left_hand.poker, self.right_hand.poker = self.right_hand.poker, self.left_hand.poker


# 先创建牌
poker1 = Poker('♣', 'K')
poker2 = Poker('♥', 'A')
# 创建手
left_hand = Hand('左手')
right_hand = Hand('右手')
# 创建人
ming = Person(left_hand, right_hand)
# 人要拿牌
ming.catch_poker(poker1, poker2)
# 看牌
ming.show()
# 交换牌
print('*' * 20)
ming.exchange_poker()
ming.show()
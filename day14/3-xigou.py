class Chicken(object):
	"""docstring for Chicken"""
	def __init__(self, name):
		super().__init__()
		self.name = name

	def jiao(self):
		print('大爷，进来玩吧')

	def __del__(self):
		# 在对象
		print('这只鸡被赎走了')

	@classmethod
	def eat(cls):
		print('吃饭')

print(100)
ji = Chicken('小豆')
print(200)
ji.jiao()
ji = 100
print(300)
		
# try:
# 	print('李白')
# 	print(100 / 10)
# 	print('杜甫')
# except Exception as e:
# 	print(e)
# else:
# 	print('这是else的代码')


# finally
'''
try:
	print('荆轲')
	print(100)
	print('刘备')
except Exception as e:
	print(e)
finally:
	print('这是finally的语句块')
'''

# with语句
# with open('test.txt', 'w', encoding='utf8') as fp:
# 	fp.write('hehe')

# raise
try:
	print('李白')
	print(a)
	print('杜甫')
except Exception as e:
	print(100)
	raise e
	print(200)
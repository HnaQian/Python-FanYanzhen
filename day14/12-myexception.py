# 自定义异常处理类
class MyException(Exception):
	def __init__(self, msg):
		super().__init__(msg)
		self.msg = msg

	def __str__(self):
		return self.msg

	def hanlde(self):
		print('自己处理异常的方法')

try:
	print('代码开始')
	# 特定的情况，抛出自己的异常
	raise MyException('自己的异常类型')
	print('代码结束')
except MyException as e:
	print(e)
	print('处理我的异常')
	e.hanlde()
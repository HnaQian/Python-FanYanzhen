# from girl import GirlFriend
import pickle

# 通过pickle方式读入程序中
fp = open('li1.txt', 'rb')
lt = pickle.load(fp)
fp.close()

print(type(lt))
# for girl in lt:
# 	print(girl)

# g1 = lt[0]
# g1.song()

'''
# 自己的字典读入方式
fp = open('li.txt', 'r', encoding='utf8')
string = fp.read()
dic = eval(string)
fp.close()

# print(dic)
li = GirlFriend(dic['name'], dic['age'], dic['face'], dic['height'], dic['weight'], dic['skin'])

print(li)
'''
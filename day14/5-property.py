class Hero(object):
	def __init__(self, name):
		self.name = name
		self.__age = 100
		self.__height = 180

	# 简化get方法
	@property
	def age(self):
		return self.__age
	# 简化set方法
	@age.setter
	def age(self, new_age):
		self.__age = new_age

yase = Hero('亚瑟')
# print(yase.age)
# yase.age = 500
# print(yase.age)

# print(yase.age())
# yase.set_age(500)
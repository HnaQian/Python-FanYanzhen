class Animal(object):
	pass

class BuRu(Animal):
	pass

class Pig(BuRu):
	color = '红色'
	siliao = '希望'
	"""猪类的详细说明"""
	def __init__(self, name, age, height):
		super().__init__()
		self.name = name
		self.age = age
		self.__height = height

	def hengheng(self):
		print('哼哼')

	@classmethod
	def eat(cls):
		print('类方法')

	@staticmethod
	def jing():
		print('静态方法')

# print(type(Pig.__name__))
# bajie = Pig('猪悟能', 3, 1)
# print(bajie.__dict__)
# print(bajie.__name__)

# 你的女儿是我的女儿的妈妈  请问我是你的谁？  女儿或者女婿
# print(Pig.__dict__)
print(Pig.__bases__)
'''
豆豆殴打小动物
狗、猫、猪、老虎   bebeat
'''
class Animal(object):
	def __init__(self, name):
		self.name = name

	# 父类只规定子类要实现的函数，自己不实现
	def bebeat(self):
		pass

class Dog(Animal):
	def bebeat(self):
		print('汪汪的冲向你咬你屁股')

class Cat(Animal):
	def bebeat(self):
		print('回头瞪你一眼，喵喵的跑了')

class Pig(Animal):
	def bebeat(self):
		print('哼哼的拱向你')

class Tiger(Animal):
	def bebeat(self):
		print('吃了你')

class Person(object):
	def __init__(self, name):
		self.name = name

	def beat(self, animal):
		print('豆豆开始殴打小动物')
		animal.bebeat()

doudou = Person('豆豆')
p = Pig('八戒')
t = Tiger('老虎')
doudou.beat(t)
def add(a, b, c=3):
	print(a + b + c)
	print(a, b, c)

# add(1, 2, 3)
# add(1, 2)

# c有两种甚至3中可能，有时候是3，有时候是5，这个时候使用默认值搞不定了，我们使用偏函数
from functools import partial
# add3 = partial(add, c=3)
# add5 = partial(add, c=5)
add1 = partial(add, 1, 2)
add2 = partial(add, 1)
add2(10, 5)

# print(add3)
# add3(100, 200)
# add5(100, 200)
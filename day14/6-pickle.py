from girl import GirlFriend
import pickle

li = GirlFriend('李孝利', 23, '耐看型', 158, 90, '微白')
fen = GirlFriend('惠芬', 25, '漂亮', 162, 95, '雪白')
fang = GirlFriend('小芳', 30, '成熟', 160, 103, '一般')
# li.song()
# 保存的时候，只能保存对象的属性

# lt = [li, fen, fang]
# 用pickle写入
fp = open('li1.txt', 'wb')
pickle.dump(fen, fp)
fp.close()

'''
# 用字典写入
# 获取对象的所有属性，返回一个字典
dic = li.__dict__
# 将字典转化为字符串
string = str(dic)
# 打开文件
fp = open('li.txt', 'w', encoding='utf8')
# 写入文件
fp.write(string)
# 关闭文件
fp.close()
'''
		
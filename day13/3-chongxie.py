class Person(object):
	def work(self, lala):
		print('person的工作方法是个%s' % lala)

class Father(Person):
	def __init__(self, name, age, height):
		self.name = name
		self.age = age
		self.height = height

	def work(self, hours):
		super().work('大傻瓜')
		print('我每天工作%s个小时' % hours)

	def jump(self):
		print('我立定跳远能跳2米')

class Son(Father):
	# 子类重写父类的方法
	def jump(self, number):
		print('我立定跳远能跳%s米' % number)

	def work(self, hours):
		# 先执行父类的功能
		# print('我每天工作12个小时')
		# 需要手动调用父类的方法,如下方式调用的是直接父类的方法
		# super().work(hours)

		# 如下方式调用的是指定父类的方法
		# Person.work(self, '大傻瓜')
		Father.work(self, hours)
		
		# 然后再增加自己的功能
		print('我和隔壁小芳建立了深刻的革命友谊')



# fu = Father('亚瑟王', 100, 176)
# fu.jump()
# fu.work(12)

yase = Son('亚瑟', 80, 180)
yase.work(8)
# yase.jump(100)
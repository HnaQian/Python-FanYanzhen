
class Animal(object):
	def my_sleep(self):
		print('喜欢睡到自然醒')

class Person(Animal):
	def __init__(self, name, age):
		print('haha')
		self.name = name
		self.age = age

	def eat(self):
		print('喜欢吃红烧茄子')

	def drink(self):
		print('喜欢喝牛栏山二锅头')

class Girl(Person):
	pass

xiaomei = Girl('高丽丽', 16)
xiaomei.eat()

# ming = Person('黄晓明', 40)
# ming.my_sleep()
# ming.eat()
# ming.drink()

class Pig(object):
	# 类属性
	age = 1
	# 对象方法
	def __init__(self, name):
		# 对象属性
		self.name = name

	@classmethod
	def gong(cls):
		# print(cls)
		# p = cls('净坛使者')
		# p.heng()
		# print(cls.age)
		# print(self.name)
		print('喜欢拱又白又嫩又大的白菜')

	@staticmethod
	def eat():
		print('喜欢吃希望饲料')

	def eat(self):
		print('对象方法的eat')


	def heng(self):
		print('哼哼哼哼哼哼')

# Pig.eat()
# print(Pig)
# Pig.gong()
bajie = Pig('猪八戒')
bajie.eat()
# bajie.gong()
# bajie.name
# bajie.gong()
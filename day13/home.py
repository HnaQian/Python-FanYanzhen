# 1

class Hand(object):

	def __init__(self, left, right):
		self.left = left
		self.right = right

	def jiaohuan(self):
		print('原来左右手的牌为%s和%s, 交换后为%s和%s' % (self.left, self.right, self.right, self.left))


# shou = Hand('红桃A', '黑桃K')
# shou.jiaohuan()

# 2

class Cat(object):
	def __init__(self, nickname, height, color, age):
		self.nickname = nickname
		self.height = height
		self.color = color
		self.age = age
	def jiao(self):
		print('我会叫')

	def jump(self):
		print('我会跳')
	
	def eat(self):
		print('我会吃鱼')

	def __str__(self):
		return '我的昵称为%s, 身高为%s, 颜色为%s, 年龄为%s' % (self.nickname, self.height, self.color, self.age)


class Tiger(Cat):
	def eat(self):
		super().eat()
		print('我会吃肉')

class Lion(Cat):
	def eat(self):
		print('我会吃肉')

mao = Cat('阿小', 20, '白色', 2)

laohu = Tiger('大明', 30, '斑点', 3)

shizi = Lion('太浪', 40, '棕黄', 4)

print(mao.__str__())
print(laohu.__str__())
print(shizi.__str__())

# 3
class ChaXun(object):

	def __init__(self, bian):
		self.bian = bian

	def youbian(self):
		path = r'E:\Python\Day09/youbian.txt'
		fp = open(path, 'r', encoding='utf8')
		line_list = fp.readlines()
		fp.close()
		# 定义一个空字典
		dic = {}
		# print(line_list)
		# 遍历line_list  将里面每一个列表字符串转化为列表
		for line in line_list:
			# 将字符串后面的无用字符删除掉
			line = line.rstrip(',\n')
			# 将line字符串转化为一个列表
			lt = eval(line)
			dic[str(lt[0])] = lt[1]
		if self.bian in dic:
			return dic[self.bian]
		else:
			return '没有这个遍'

# 4
class ZiDian(object):
	def __init__(self, word):
		self.word = word

	def traslate(self):
		
		print('汉语意思为%s' % )



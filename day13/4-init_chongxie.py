class Girl(object):
	def __init__(self, name, age):
		self.name = name
		self.age = age

class QianRen(Girl):
	def __init__(self, name, age, height, face):
		# 去手动调用父类的构造方法，让父类初始化父类的那一部分
		super().__init__(name, age)
		self.height = height
		self.face = face

	def say(self):
		print('我叫%s' % self.name)

# hua = QianRen('王美丽', 18, 160, '俊俏')
# hua.say()

class ClassName(object):
	"""docstring for ClassName"""
	def __init__(self, arg):
		super().__init__()
		self.arg = arg

		
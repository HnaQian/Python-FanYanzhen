class Rectangle(object):
	def __init__(self, length, width):
		self.length = length
		self.width = width

	def area(self):
		return self.length * self.width

	def zhouchang(self):
		return 2 * (self.width+self.length)

# r = Rectangle(5, 4)
# print('面积为%s' % r.area())
# print('周长为%s' % r.zhouchang())

import time
class Car(object):
	def __init__(self, speed):
		self.speed = speed

	# 公路作为对象进行传递参数
	def run(self, road):
		# 获取得到公路的长度
		l = road.length
		# 模拟跑的过程
		while l:
			l -= self.speed
			print('骚粉宝马奔跑在航海路上，还剩%s千米' % l)
			time.sleep(1)
		print('汽车跑完整个航海大道耗时%s小时' % (road.length / self.speed))

class Road(object):
	def __init__(self, length):
		self.length = length

hanghai = Road(1000)
bmw = Car(100)
bmw.run(hanghai)

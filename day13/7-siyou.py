class Girl(object):
	def __init__(self, name, age):
		self.name = name
		# 如果这么写了，那么这个__age就是一个私有属性
		self.__age = age

	# 获取年龄的方法  get方法
	def get_age(self):
		return self.__age

	# 设置年龄的方法  set方法
	def set_age(self, new_age):
		self.__age = new_age

	# 这个方法就是私有方法
	def __flower(self):
		print('喜欢百合花')

	def hua(self):
		print('这是hua方法')
		self.__flower()

xiaofen = Girl('柳慧芬', 23)
xiaofen.hua()
# xiaofen._Girl__flower()
# xiaofen.set_age(24)
# print(xiaofen.get_age())
# xiaofen.__age = 24
# print(xiaofen.get_age())
# print(xiaofen.name)
# print(xiaofen.__age)
# print(xiaofen._Girl__age)
class Wolf(object):
	# 写到这里的叫做类属性
	ni = '狗蛋'
	def __init__(self, name, age):
		# 只要写到这里面的都是成员属性，依赖对象，必须有对象才能使用成员属性
		self.name = name
		self.age = age
	def wo(self):
		print('喔喔喔')

# 如何调用类属性
# Wolf.ni = '毛蛋'
# print(Wolf.ni)
w = Wolf('凶', 4)
w.ni = '毛蛋'
print(w.ni)
print(Wolf.ni)
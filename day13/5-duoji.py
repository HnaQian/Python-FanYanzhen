class Animal(object):
	def eat(self):
		print('动物吃东西的方法')
		
class Lv(Animal):
	# def __init__(self, name):
	# 	self.name = name

	def naili(self):
		print('推磨3天3夜不嫌累，喝口红牛接着干')

	def eat(self):
		print('喜欢吃豆干')

class Horse(Animal):
	def __init__(self, height):
		self.height = height

	def baofa(self):
		print('百公里加速只需要5秒')

	def eat(self):
		print('喜欢吃青青草')

class Luo(Lv, Horse, Animal):
	# def __init__(self, name):
	# 	self.name = name

	def eat(self):
		# super().eat()
		Horse.eat(self)
		print('喜欢吃猪饲料')

heizi = Luo()
heizi.eat()
# heizi.baofa()
# heizi.naili()

# 查看执行方法时候的顺序
print(Luo.__mro__)
		
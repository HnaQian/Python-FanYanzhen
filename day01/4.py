# a = 'l love you" baby hello'
# b = "清明时节雨纷纷'，路上行人欲断魂，借问酒家何处有，牧童遥指杏花村"
# c = '<div class="baby">你好，我来自大郑州</div>'

# d = '昨天晚上，我一个人走在路上，\
# 突然间一个穿短裙的女孩像我奔跑过来，\
# 然后我说，怎么了，谁欺负你了，她色眯眯的看着我说，\
# 俺想和处一段，你看咋样'

# e = '北国风光，千里冰封' \
# 	'万里雪飘，望长城内外' \
# 	'惟余莽莽，大河上下，顿失滔滔' 

# f = '''山舞银蛇，原驰蜡象
# 欲与天公试比高
# 须晴日，看红装素裹'"，分外妖娆
# 江山如此多娇，引无数英雄竞折腰
# '''
# g = """惜秦皇汉武，略输文采
# 唐宗宋祖，稍逊风骚
# 一代天骄，成吉思汗"''
# 只识弯弓射大雕，俱往矣，数风流人物
# 还看今朝
# """
# h = 'i love\' you'
# i = "我爱你\"中国"
# a = '我喜欢的歌星叫做\\n刘德华'

# path = r'C:\Users\ZBLi\Desktop\1803\day02'

# # print(path)
# # print('你好，\\t王宝强，\t你的媳妇叫做马蓉蓉')
# # print('你好啊，宋喆小弟，\t你的媳妇也叫做马蓉蓉')

# a = 'https://www.baidu.com/?'
# b = 'tn=62095104_2_oem_dg'
# a = 'hello'
# # b = 100
# # c = a + '.mp4'
# # print(c)

# # a = '柳岩' * 8
# # print(a)

# a = '商女不知亡国恨,隔江犹唱后庭花'
# a = 'hello baby'
# # a[1] = '男'
# # print(a[1])

# # print(a[::-1])

# # print(a[0:6:2])
# # print(a[-3::-1])
# # print(a[-1:-4:-1])
# # print(a[-4:-1:1])

# print(a[-15])
# print(a[:3])
# print(a[-3:])
# print(a[2:-2])
# print(a[:])

# name = '刘德华'
# like = '关之琳'
# age = 60
# zhuanye = '演员'
# height = 173.563
# string = '我叫%s, 我喜欢%s, 我的年龄为%s, 我的专业为%s' % ('周杰伦', '蔡依林', 37, '国语音乐')
# string = '我叫%s, 我喜欢%s, 我的年龄为%010d, 我的身高为%010.3f' % (name, like, age, height)

# print(string)

# a = 100
# print('我的大小为%o' % a)
# print('我的大小为%x' % a)

# string = '我叫{}, 我喜欢{}, 我的年龄为{}'.format('林俊杰', '田馥甄', 37)
# string = '我叫{2}, 我喜欢{1}, 我的年龄为{0}'.format('林俊杰', '田馥甄', 37)
string = '我叫{name}, 我喜欢{like}, 我的年龄为{age}'.format(name='林俊杰',age=37, like='田馥甄')
print(string)

# print(string)

# print('haha', end='')
# print('hehe', end='')
# print('')
# print('heihei')
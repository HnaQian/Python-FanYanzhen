# 1、实现列表中count函数，my_count
# count 返回字符串1在字符串2中出现的次数
# def my_count(lt, a):
# 	i = 0
# 	for x in lt:
# 		if x == a:
# 			i += 1
# 		else:
# 			pass
# 	return i

# 2、实现列表中index函数，my_index
	# 自己实现
# def my_index(lt, a):
	# 找到元素第一次出现的位置
# 	i = 0
# 	for x in lt:
# 		if x == a:
# 			return i
# 		i += 1
# 	return -1
# print(my_index([1, 2, 'a', 6, 11], 12)) 
# 3、给一个字符串，将里面的大小写转化（自己swapcase函数/大小写转换）
# def my_string(a):
# 	b = ''
# 	for x in a:
# 		if (ord(x) >= 65) and (ord(x) <= 90):
# 			x = chr(ord(x) + 32)
# 		elif (ord(x) >= 97) and (ord(x) <= 122):
# 			x = chr(ord(x) - 32)
# 		else:
# 			pass
# 		b += x
# 	return b
# 4、将字符串："name=goudan&age=18&pwd=123&height=180"，转化成python 字典：
# {'name':'goudan', 'age':'18', 'pwd':'123', 'height':'180' }
# def zhuanhuan():
# 	dic = {}
# 	string = "name=goudan&age=18&pwd=123&height=180"
# 	lt = string.split('&')
# 	# print(lt)
# 	for x in lt:
# 		lt1 = x.split('=')
# 		dic[lt1[0]] = lt1[1]
# 	return dic


# 5、将字典拼接成指定格式的字符串
# 	{'name':'goudan', 'age':'18', 'pwd':'123', 'height':'180' }  拼接为
# 	"name=goudan&age=18&pwd=123&height=180"

# def pinjie():
# 	string = ''
# 	dic = {'name':'goudan', 'age':'18', 'pwd':'123', 'height':'180' } 
# 	for k, v in dic.items():
# 		string += (k + '=' + v + '&')
# 	return string.rstrip('&')
# 6、将字符串中的所有数字字符体取出来，组成新的字符串
# 方法1：
# def shuzi(string):
# 	a = ''
# 	for x in string:
# 		if (ord(x) >= 48) and (ord(x) <= 57):
# 			a += x
# 		else:
# 			pass
# 	return a


# 7、将字符串中出现的所有数字相加求和得到结果
# 	'a%&bc1234lala12hah%$a56'   结果为1234+12+56=1302
# 方法1
# def my_sum(string):
# 	a = '0'
# 	he = 0
# 	for x in string:
# 		if (x >= '0') and (x <= '9'):
# 			a += x
# 		else:
# 			he += int(a)
# 			a = '0'
# 	if a != '0':
# 		he += int(a)
# 	return he
# 方法二：
# def my_sum(string):
# 	ret = ''
# 	for x in string:
# 		# 判断如果不是数字，则将其变成=号
# 		if not x.isdecimal():
# 			x = '='
# 		ret += x
# 	ret_list = ret.split('=')
# 	he = 0
# 	for x in ret_list:
# 		# if后面的条件句，如果是单独的元素，系统将自动转换成bool值进行判断.
# 		if x:
# 			he += int(x)
# 	return he

# 	# print(ret_list)

# print(my_sum('a%&bc12lala12hah%$a56'))

# print(my_sum('1ajaj17817jajd3a'))
# 8、生成随机字母数字混合验证码，输入4生成4位，输入6生成6位
# import random
# 方法一：
# def suiji(number):
# 	string1 = '0123456789'
# 	string2 = 'abcdefghijklmnopqrstuvwxyz'
# 	string3 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
# 	string4 = string1 + string2 + string3
# 	string5 = ''
# 	for x in range(0, number):
# 		string5 += random.choice(string4)
# 	return string5
# print(suiji(4))
# 方法二：
# def suiji(number):
	# 用到快速列表生成式。
# 	shuzi_list = [str(x) for x in range(10)]
# 	daxie_list = [chr(x) for x in range(97, 123)]
# 	xiaoxie_list = [chr(x) for x in range(65, 91)]
# 	zong_list = shuzi_list + daxie_list + xiaoxie_list
# 	ret = ''
# 	for x in range(0, number):
# 		ret += random.choice(zong_list)
# 	return ret
# print(suiji(4))

# 9、打印图形
# 输入4，打印如下图形
#    *       空格的个数  number-(x+1) 	*的个数			1 + 2*0
#   ***					number-(x+1)	        		1 + 2*1
#  *****					number-(x+1)				1 + 2*2
# *******					number-(x+1) 				1 + 2*3
# 方法一
# def tuxing(number):
# 	# 1/3/5/7排列,定义一个空格字符
# 	for x in range(0, number):
# 		ret = ' '*(number - (x+1))
# 		# 每次打印*的个数,每次空格的次数
# 		print(ret + '*'*(1 + 2*x))
# 方法二
# def tuxing1(number):
# 	ret = ''
# 	# center(), 居中显示，两边补字符
# 	for x in range(0, number):
# 		ret = (1 + 2*x) * '*'
# 		print(ret)

# tuxing1(5)
# def baby(face, skin):
# 	print('脸蛋%s, 皮肤%s' % (face, skin))
# lt = ['漂亮', '白皙']

# 拆包列表
# # baby(*lt)
# # 拆包字典
# dic = {'face': '漂亮', 'skin': '白皙'}
# baby(**dic)




















			






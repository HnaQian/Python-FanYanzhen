def my_hex(number):
	print('我是函数中的语句')
	string = ''
	# 先定义一个字典，用来转化大于10的这些哥们
	dic1 = {10: 'a', 11: 'b', 12: 'c', 13: 'd', 14: 'e', 15: 'f'}
	while number:
		# 对number求商取余
		number, yushu = divmod(number, 16)
		# 还得对余数进行判断，因为a-f这么一个东西

		# 判断yushu这个键在不在字典中，在的话提取值
		# 第三种方式
		if yushu in dic1:
			yushu = dic1[yushu]

		string += str(yushu)
	return '0x' + string[::-1]

# print('这是程序的第一步')
# print(my_hex(200))
# print('这是程序的最后一步')

def main():
	print('我是函数执行前的语句')
	a = my_hex(200)
	print('我是函数执行后的语句')
	if a == '0xc8':
		print('+10')
	else:
		print('0')

# 程序的入口函数，程序起来之后运行的第一个函数就是main
main()
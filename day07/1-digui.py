# 写一个函数，传递n，打印n遍我爱你中国
# def xxx(n):
# 	for x in range(1, n + 1):
# 		print('我爱你中国')

def xxx(n):
	# 判断临界条件
	if n == 1:
		print('我爱你中国')
		# 下面这个return必须加，否则，递归就会陷入死循环
		return
	# 假设这个函数可以使用
	xxx(n - 1)
	print('我爱你中国')

# xxx()

'''
缺点：一瞬间占用内存太大，如果递归太多，程序立马挂了
xxx(1)
xxx(2)
	xxx(1)
	print()

xxx(3)
	xxx(2)
		xxx(1)
			print()
		print()
	print()

xxx(10)
'''

# def jie(n):
# 	ret = 1
# 	for x in range(1, n + 1):
# 		ret *= x
# 	return ret

def jie(n):
	if n == 0:
		return 1
	return jie(n - 1) * n

print(jie(50))
# 4个字节  32位  
def baby():
	print('请君暂上凌烟阁,若个书生万户侯')

def goudan(fn):
	# print(fn)
	print('goudan函数的开始')
	fn()
	print('goudan函数的结尾')

# goudan(baby)
# goudan(fn=baby)
# lt = [baby]
# goudan(*lt)
# dic = {'fn': baby}
# goudan(**dic)

# baby = 100
# print(type(baby))
# baby()

# print(baby)
# haha = baby
# print(type(haha))
# haha()


# print(type(baby))
# a = 100
# a()
# print(a)
# a = 'hellobaby'
# print(a)
# a = ['勇士', '美女']
# print(a)
# a = ('野兽', '美女')
# print(a)

# 简单计算器例子
def add(a, b):
	return a + b

def sub(a, b):
	return a - b

# def mul(a, b):
# 	return a * b

def div(a, b):
	return a / b

def calcu(a, b, fn):
	return fn(a, b)

# print(calcu(5, 5, div))

# print(calcu(5, 5, lambda x, y: x * y))
print(calcu(5, 5, lambda x, y: x - y))

# a = lambda x: x * x
# b = lambda x, y: x - y
# print(type(a))
# print(a(5))
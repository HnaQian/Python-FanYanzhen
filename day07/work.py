# 第一题
def my_hex(number):
	string = ''
	# 先定义一个字典，用来转化大于10的这些哥们
	dic1 = {10: 'a', 11: 'b', 12: 'c', 13: 'd', 14: 'e', 15: 'f'}
	while number:
		# 对number求商取余
		number, yushu = divmod(number, 16)
		# 还得对余数进行判断，因为a-f这么一个东西

		# 判断yushu这个键在不在字典中，在的话提取值
		# 第三种方式
		if yushu in dic1:
			yushu = dic1[yushu]
		'''
		# 第二种方式
		# 10 == a(97)
		# 11 == b(98)
		if yushu > 9:
			yushu = chr(ord('a') - 10 + yushu)
		'''

		'''
		# 第一种方式
		if yushu == 10:
			yushu = 'a'
		elif yushu == 11:
			yushu = 'b'
		elif yushu == 12:
			yushu = 'c'
		elif yushu == 13:
			yushu = 'd'
		elif yushu == 14:
			yushu = 'e'
		elif yushu == 15:
			yushu = 'f'
		'''
		string += str(yushu)
	return '0x' + string[::-1]

def my_hex2(number):
	return '0x%x' % number

# print(my_hex(200))
# print(my_hex2(200))
# print(hex(200))

import random
'''
3, 6
0-1之间的随机数 * 3
0-3 + 3 == 3-6
'''
def my_randint(a, b):
	# 随机获取0-1之间的一个小数
	suiji = round(random.random())
	# 先求出b-a的差值
	delta = b - a
	# 求随机值
	number = a + suiji*delta
	return number

# print(my_randint(1, 6))

def code(number):
	'''
	说明：给一个位数，返回该位数的数字验证码
	'''
	string = '0123456789'
	ret = ''
	# 从中随机抽取number个
	for x in range(number):
		c = random.choice(string)
		ret += c
	return ret
# print(code(5))

import sys
def jisuanqi():
	# 获取通过程序传递给我的参数
	lt = sys.argv
	# 获取第一个和第二个数以及符号
	number1 = float(lt[1])
	number2 = float(lt[3])
	fuhao = lt[2]
	# 判断符号，进行对应的运算
	if fuhao == '+':
		return number1 + number2
	elif fuhao == '-':
		return number1 - number2
	elif fuhao == '*':
		return number1 * number2
	elif fuhao == '/':
		return number1 / number2

# print(jisuanqi())

# lt = ['周杰伦', '蔡依林', '昆凌', '凤凰传奇', '周杰伦']

# del lt[0]

# print(lt[0])

def shanchu(lt, a):
	'''
	说明：给一个列表，给一个a，将列表中所有的a全部删除掉
	'''
	# 遍历这个列表，所有的元素依次和a进行比较
	# 1 2 3 3 3 5
	# 1 2 3 3 5
	i = 0
	while i < len(lt):
		if lt[i] == a:
			del lt[i]
			i -= 1
		i += 1
	return lt

	# 判断x不是a的时候，添加到新的列表中
	# ret = []
	# for x in lt:
	# 	if x != a:
	# 因为列表只能和列表相加，所以将x先放到列表中即可
	# 		ret += [x]
	# return ret
'''
[1, 2, 3, 3, 5]
'''
# print(shanchu([1, 2, 3, 3, 3, 5], 3))

def cai():
	while 1:
		# 给以提示，是否接着玩
		flag = input('是否继续,请指示(y继续n退出):')
		if flag == 'n':
			break
		# 让电脑出一个
		pc = random.randint(1, 3)
		# 提示让用户输入
		person = int(input('请输入(1石头2剪刀3布):'))
		# 判断谁赢了
		if pc == person:
			print('你和电脑旗鼓相当')
		elif (person == 1 and pc == 2) or (person == 2 and pc == 3) or (person == 3 and pc == 1):
			print('你真厉害')
		else:
			print('电脑赢了')

# cai()

def nixu(string):
	# return string[::-1]
	# hello
	# 倒着遍历，依次将每个字符放到新的字符串中
	ret = ''
	# 先求出字符串的长度
	# string[0]  string[1]  string[5]
	# string[5]  string[4]  string[3]  string[2]  string[1]  string[0]
	length = len(string)
	i = length - 1
	while i >= 0:
		ret += string[i]
		i -= 1
	return ret
	# for x in range(length - 1, -1, -1):
	# 	pass

# print(nixu('大伟真坚强'))

def guishu():
	string = """5582|1860101|010|北京市|北京联通GSM卡
5583|1860100|010|北京市|北京联通GSM卡
5584|1368141|010|北京市|北京移动神州行卡
5585|1860111|010|北京市|北京联通GSM卡
5586|1358198|010|北京市|北京移动动感地带卡
5587|1361139|010|北京市|北京移动预付费卡
5588|1361138|010|北京市|北京移动神州行卡
5591|1360110|010|北京市|北京移动全球通卡
5748|1364110|010|北京市|北京移动神州行卡
10186|1581584|020|广东省广州市|广东移动全球通卡
15046|1391897|021|上海市|上海移动全球通卡
17250|1502207|022|天津市|天津移动全球通卡
21137|1345272|023|重庆市万州|重庆移动大众卡
22700|1347812|024|辽宁省沈阳市|辽宁移动大众卡
24256|1377065|025|江苏省南京市|江苏移动全球通卡
26360|1898606|027|湖北省武汉市|湖北电信CDMA卡
28709|1860802|028|四川省成都市|四川联通GSM卡
30641|1552961|029|陕西省西安市|陕西联通GSM卡
31700|1563007|0310|河北省邯郸市|河北联通GSM卡
33360|1583396|0311|河北省石家庄市|河北移动全球通卡
34825|1508122|0312|河北省保定市|河北移动全球通卡
35363|1551235|0313|河北省张家口|河北联通GSM卡
37700|1331326|0316|河北省廊坊市|河北电信CDMA卡
43500|1350358|0358|山西省吕梁市|山西移动全球通卡
43908|1553625|0359|山西省运城市|山西联通GSM卡
44521|1335360|0370|河南省商丘市|河南电信CDMA卡
50078|1509369|0378|河南省开封市|河南移动全球通卡
53603|1583981|0398|河南省三门峡|河南移动全球通卡
53916|1335897|0410|辽宁省铁岭市|辽宁电信CDMA卡
55248|1554254|0411|辽宁省大连市|辽宁联通GSM卡
58618|1374272|0427|辽宁省盘锦市|辽宁移动全球通卡
58932|1554183|0429|辽宁省葫芦岛|辽宁联通GSM卡
60268|1340475|0431|吉林省长春市|吉林移动大众卡"""
	# 思路：解析字符串，将1340475作为键，将吉林移动大众卡作为值保存到字典中，然后根据用户的输入，去字典中查找有没有这个键值对，有的话返回对应的值即可，没有的话，返回查无此号码
	# 先定义一个空字典
	dic = {}
	# 解析字符串,将字符串按照换行符进行切割
	lt = string.splitlines()
	# print(lt)
	# 处理每一行数据，其实就是遍历列表，处理列表中每一个元素即可
	for line in lt:
		# 写一个处理方式，因为处理每一行的方式都是一样的
		# 将每一行首先按照 | 进行切割
		info_lt = line.split('|')
		# print(info_lt)
		# break
		# 提取固定的信息存放到字典中
		phone = info_lt[1]
		area = info_lt[-1]
		dic[phone] = area

	# 打印这个字典看看对不对
	# print(dic)
	while 1:
		flag = input('y继续n退出')
		if flag == 'n':
			break
		in_phone = input('请输入要查询的手机号码:')
		# 对手机号进行处理
		if len(in_phone) > 7:
			in_phone = in_phone[:7]
		elif len(in_phone) < 7:
			print('输入的手机号码格式不对')
			continue
		else:
			pass
		# 判断in_phone在不在字典中
		if in_phone in dic:
			print(dic[in_phone])
		else:
			print('查无此号码')

guishu()
day10-文件、时间、日期、模块

1、文件
	【注】无论这个文件的是读还是写，在操作文件的时候都会有一个文件指针在移动
	w+ : 在w的基础上多了一个读取的功能
		清空文件，文件指针在最前面
	r+ : 在r的基础上多了一个写入的功能
		文件指针在最前面
	a+ : 在追加的基础上多了一个读取的功能
		文件指针在最后

	带b的打开方式，b就是字节的意思，如下意思就是当读写的时候只能使用字节类型进行读写
	【注】以带b的方式打开的时候，后面不能加encoding
	wb : 以只写方式打开，但是只能写入字节类型的，如果要写字符串，要先将字符串转化为字节然后再写入
	rb : 以只读方式打开，但是读取过来，都是字节类型
	ab : 以追加方式打开，只能写字节类型
	wb+ : 在wb的基础上，多了一个读取的功能
	rb+ : 在rb的基础上，多了一个写入功能
	ab+ : 在ab的基础上，多了一个读取功能

	fp.seek() : 移动文件指针
		【注】必须以b模式打开，才能移动文件指针，移动的单位是字节
		offset  ：  偏移量
		相对位置 ： 
			0 ： 相对于文件的开头
			1 ： 相对于当前位置
			2 ： 相对于文件的末尾
	fp.tell() : 告诉指针当前的位置
2、时间、日期、日历、hashlib
	import time
	sleep(seconds) : 休眠几秒
	time() : 时间戳
	localtime() : 返回一个时间元组
		(tm_year=2018, tm_mon=4, tm_mday=28, tm_hour=10, tm_min=33, tm_sec=8, tm_wday=5, tm_yday=118, tm_isdst=0)
		tm_wday ： 周内天数，0-6
		tm_yday :  一年内天数
	strftime() ：将一个时间元组转化为指定格式的时间
		%Y : 年份 四位
		%y : 年份 两位
		%m : 月份 01-12
		%d : 几号 01-31
		%H : 小时 00-24
		%I ：小时 00-12
		%M ：分钟 00-59
		%S : 秒数 00-59
	mktime() : 将一个时间元组，转化为时间戳
		时间元组是9个值，前六个必须写，后面3个都写0就行了
	gmtime() : 给一个时间戳，返回时间元组，得到的时间为格林威治时间
		东八区 ：需要在0时区的基础上加8个小时
		格林威治时间 ：0时区的时间
	localtime() ： 如果不传递参数，获取当前时间的时间元组，如果传递参数，获取指定时间的时间元组，得到的是当前时区的时间元组
	asctime() ： 给一个时间元组，返回指定格式的时间字符串，如果不给，默认为当前时间
	timezone ：0时区-当前时区秒数的差值

	日期模块
	import datetime
	dt_now = datetime.datetime.now()   当前日期和时间的日期时间字符串
	dt_ziding = datetime.datetime()    获取指定日期和时间的日期时间字符串
	dt.strftime() 转化为指定格式的日期和时间
	dt.date() ：获取日期对象中的日期
	dt.time() ：获取日期对象中的时间
	dt.timestamp() : 获取日期对象的时间戳
	dt.hour\minute\second : 获取日期对象的时间信息
	datetime.datetime.fromtimestamp() : 给一个时间戳，返回指定的日期对象
	datetime.timedelta() : 参数有  days, hours, minutes, seconds, 获取一个日期差值对象，这个对象可以直接和日期对象进行加减操作

	重点：
	（1）知道什么是时间戳
	（2）知道什么是东八区、0时区
	（3）时间戳和日期时间的相互转化
	（4）如何转化为指定格式时间
	（5）日期对象的加减操作

	日历模块
	import calendar
	calendar(year, w=2, l=1, c=6)  查看指定年份所有日历
	isleap(year)   : 判断这个年份是不是闰年
	leapdays(y1, y2) : 计算y1-y2之间所有的闰年
	month(year, month, w=2, l=1)  查看指定年份月份的日历

	hashlib模块
		md5加密：加密过后，生成一串32位的字符串
			指定字符串加密生成的都是唯一的32位字符串
	hash = hashlib.md5()
	hash.update('admin'.encode('utf-8'))
	tmp = hash.hexdigest()
3、模块
	胶水语言，有很多其它的第三方模块供你使用，但是这些模块需要你首先安装
	定义：模块就是工具包，里面有好多的函数，要使用模块里面的函数，需要首先导入这个模块， import
	模块分类：
		（1）Python标准库模块
			random  os  time  datetime  re
		（2）第三方模块
			别人写好的模块，你可以直接拿来使用，但是前提是需要安装这个模块
			官方给我们提供了一个安装工具  pip
			步骤：首先安装pip，然后使用pip安装第三方模块
			查看有没有安装pip：终端输入  pip -V
			windows下的pip是安装Python的时候默认安装的
			linux下的pip是需要手动安装的
		（3）自定义模块
			自己写的模块，自己写了好多函数，想当做一个模块进行调用
	模块的导入方式
		导入模块：
			import random
			import time
			import 模块名
			【注】一般导入一个模块使用一行
			使用模块中的函数格式：
				模块名.函数名()
		精确导入：
			from 模块名 import 函数名、全局变量名、类名
			使用方式：
				函数名()
				全局变量名
				类名()
		模糊导入：
			from random import *
			将random模块中所有的函数、全局变量、类全部导入
			使用方式和精确导入使用方式相同
			【注】不要使用这种方式
		起别名
			给模块起别名
				import random as r
				r.randint()   只能使用别名了，原名不能使用
			给函数起别名
				from random import randint as rd
				rd(1, 5)   只能使用别名了，原名不能使用
		自定义模块
			哥们，你写的每一个py文件都是一个模块，模块名就是文件名
			自己写的模块是供别人、自己使用，只有使用的时候，会去调用里面的函数，让这些函数执行
			使用自定义模块方式和上面一模一样
			pycache：缓存文件架，提高执行效率的，没有意义
		测试模块
			当模块作为主程序执行的时候，__name__的值是__main__，当模块作为模块被其它程序调用的时候，__name__的值就是当前模块名，所以在程序的最下面一般都要这么写代码
			if __name__ = '__main__':
				main()
	包：
		有些模块有关联，为了方便统一管理这些模块，我们会将这些模块放到一个文件夹里面，这个文件夹就称之为一个包
		# 第一种使用方式
		# import bao.zimo
		# bao.zimo.tang()

		# 第二种方式
		# from bao import zimo
		# zimo.song()

		# 第三种方式
		# from bao.zimo import minguo
		# minguo()

		__init__.py文件：这个文件一般都会出现在包里面，一般情况下，包里面必须有这个文件，这个文件用来表名这个文件夹是一个包，这个文件一般是空文件
	如何安装第三方模块
		默认pip是从国外网站上面下载安装，慢，访问失败
		国内也有很多非常好的镜像源
		豆瓣、阿里、清华、搜狐、网易等

		如何将pip源切换为国内源
		1、打开文件管理器，在上面输入 %appdata%
		2、在这里面新建一个pip文件夹
		3、在这个文件夹里面新建一个文件  pip.ini
		4、在这个文件中写入如下内容：阿里源
[global]
timeout = 6000
index-url = https://mirrors.aliyun.com/pypi/simple/
trusted-host = mirrors.aliyun.com
		【注】如果是win8或者win10的系统，需要具备管理员的权限才能操作
		
		pip常见指令
		安装： pip install 模块名
		卸载： pip uninstall 模块名
		显示所有已安装模块： pip list

		如果国内源配置成功： pip install requests
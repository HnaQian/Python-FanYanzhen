import time

# 时间戳
# 就是计算机计算时间的一种方式，都是根据时间戳计算出来，时间戳就是从1970-1-1 0:0:0 到现在的一个秒数
# print(time.time())
# print(time.localtime())

# timestamp = 1524883482.4752

# print(time.strftime('%y-%m-%d %I:%M:%S', time.localtime(timestamp)))

# print(time.mktime((2018, 4, 28, 10, 33, 8, 0, 0, 0)))
# print(time.mktime((1996, 12, 3, 7, 45, 0, 0, 0, 0)))

# 出生的时间戳
t = 849570300.0

# print(time.gmtime(t))
# print(time.time() - t)
# print(time.localtime(t))

# print(time.asctime())

print(time.timezone)
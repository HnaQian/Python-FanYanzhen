
# 第一种使用方式
# import bao.zimo
# bao.zimo.tang()

# 第二种方式
# from bao import zimo
# zimo.song()

# 第三种方式
# from bao.zimo import minguo
# minguo()
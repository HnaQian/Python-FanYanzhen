'''
# 学习w+
fp = open('test.txt', 'w+', encoding='utf8')

fp.write('花田错,花田里犯了错,说好,破晓前忘掉')

# 可以移动到文件的最开始处
fp.seek(0)
string = fp.read()

print(string)

fp.close()
'''

'''
# 学习 r+
fp = open('test.txt', 'r+', encoding='utf8')

fp.write('王力宏')

string = fp.read()
print(string)

fp.close()
'''

'''
fp = open('test.txt', 'a+', encoding='utf8')

fp.write('和slina唱了一首我们的歌')

fp.seek(0)

string = fp.read()

print(string)

fp.close()
'''

'''
fp = open('test.txt', 'wb')

string = '王力宏这几年没有新歌曲了'

fp.write(string.encode())

fp.close()
'''

# 写入图片
'''
image_url = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1524889831020&di=67db0bbb3914c477abbbe89e5f049d4d&imgtype=0&src=http%3A%2F%2Fimg5q.duitang.com%2Fuploads%2Fblog%2F201403%2F10%2F20140310120739_ULurn.jpeg'
import requests

r = requests.get(image_url)

filename = 'huola.jpeg'
# 将图片写入到文件中，是wb格式
fp = open(filename, 'wb')

fp.write(r.content)
# print(r.content)

fp.close()
'''


# 王力宏
# fp = open('test.txt', 'rb')

# print(fp.tell())

# b = fp.r


# fp.seek(-1, 1)

# print(fp.tell())

# # print(b.decode())

# fp.close()
fp = open('test.txt', 'w+', encoding='utf8')
fp.write('花田里犯了错，说好，破晓前忘掉')
fp.seek(0)
string = fp.read()
print(string)

fp.close()

fp = open('test.txt', 'r+', encoding='utf8')

fp.write('王力宏')
string = fp.read()
print(string)

fp.close()
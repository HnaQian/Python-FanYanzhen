# 里面定义的都是函数、变量、类

g_baby = 100

def tang():
	print('千山鸟飞绝,万径人踪灭,孤舟蓑笠翁,独钓寒江雪')

def song():
	print('寻寻觅觅,冷冷清清,凄凄惨惨凄凄')

def minguo():
	print('最是那一低头的温柔,恰似一江春水向东流')

# tang()
# 在这里写模块的测试代码
# tang()
# song()
# minguo()

# __main__ : 当直接执行当前py程序的时候，__name__就是main
# zimo：当这个py文件作为模块供其他py程序使用的时候，__name__ 就是模块名
# print(__name__)

def main():
	# 测试代码
	tang()
	song()
	minguo()

if __name__ == '__main__':
	main()


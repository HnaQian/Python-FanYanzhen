import re

# 学习函数，match，是re模块里面的函数，你要研究的是match需要传递什么参数，match函数给我什么样的返回值
'''
参数1：pattern  正则表达式
参数2：string   字符串
【注】match是从字符串最开头开始匹配的,只匹配一个
返回值：如果匹配成功，返回一个对象，如果匹配失败，返回None
返回一个对象，对象有两个方法
ret.group() : 匹配到的内容
ret.span() ：匹配内容的下标
'''
# ret = re.match('love', 'love you love very much')

# print(ret)
# print(ret.group())
# print(ret.span())


# search函数学习
# ret = re.search('鸭蛋', '竹外桃花三两枝,春江水暖鸭先知,你家的鸭子大不大')
# print(ret)
# print(ret.group())
# print(ret.span())


# findall函数
# ret = re.findall('sister', 'i love you, you love me, she love me, her sister love me')
# print(ret)

# compile函数
# pattern = re.compile('love')
# print(type(pattern))
# ret = pattern.match('love is a forever question')
# ret = pattern.search('i love her, love her sister')
# ret = pattern.findall('i love you, you love me ')
# print(ret)


# string = 'hello'
# print(string.upper())
# print(str.upper(string))

# class Dog(object):
# 	def wangwang(self):
# 		print('狗在叫')

# dog = Dog()
# dog.wangwang()
# Dog.wangwang(dog)
# 		
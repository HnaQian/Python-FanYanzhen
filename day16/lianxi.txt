1、写出正则，匹配前面三个正确的邮箱格式
test@qq.com
abc_meme@163.cn
dudu-lala@sina.net 
meme@qq.dudu    错误
#$%@lala.com    错误

2、写正则，匹配qq号码（5-12），没有以0开头的

3、写出正则，匹配正确的手机号
4、写出正则，匹配后五位相同的手机号
13567891010
12345678900   错误
15790908888
17930212345
19901011111
19901016161
18822366666
01010101010   错误
09456781356   错误

5、写出正则，匹配如下正确url
http://www.baidu.com
httplala://www.baidu.com     错误
http://www.163.com/index.html
https://www.sina.cn:8080/index.html?username=goudan
https://www.haha.net/index.html?username=goudan&password=1234
httpabcs://www.haha.net/index.html?username=goudan&password=1234   错误
httpabcs://www.haha.net:18dd/index.html?username=goudan&password=1234   错误
http://www.haha.net:80/index.html&username=goudan&password=1234   错误

6、提取指定内容
string = '<div><a href="http://www.baidu.com/">百度一下</a></div>'
将http://www.baidu.com/和百度一下分别提取出来
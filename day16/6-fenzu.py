import re

# pattern = re.compile(r'\d+|[a-z]+')
# pattern = re.compile(r'(\d{2}[a-z]{2}){4}')
# pattern = re.compile(r'(\d{2}|[a-z]{2}){3}')
# pattern = re.compile(r'<(\w+)><(\w+)>.*</\2></\1>')
pattern = re.compile(r'<(?P<he>\w+)><(?P<la>\w+)>.*</(?P=la)></(?P=he)>')

ret = pattern.search('lala<maodan><span>七里香是周杰伦唱的歌</span></maodan>dudu')
print(ret.group())
print(ret.group(1))
print(ret.group(2))
import re

# \d 学法
# pattern = re.compile('a\d')
# ret = pattern.search('今天是星期1a02a34')
# print(ret.group())


# \D学法
# pattern = re.compile('\D')
# ret = pattern.search('a17701256561=+-')
# ret = pattern.findall('a17701256561=+-')
# print(ret)


# \w 学习
# pattern = re.compile('\w')
# ret = pattern.search('##$#_$$$')
# print(ret.group())

# \W 学习
# pattern = re.compile('\W')
# ret = pattern.search('i-love you = you love me')
# print(ret.group())

# \s 学习
# pattern = re.compile('\s')
# ret = pattern.search('\ti-loveyou = you love me')
# print(ret.group())
# print(ret.span())

# \S 学习
# pattern = re.compile('\S')
# ret = pattern.search('\t\n=-loveyou = you love me')
# print(ret.group())

# . 学习
# pattern = re.compile('.')
# ret = pattern.search('\n=-loveyou = you love me')
# print(ret.group())
# print(ret.span())

# [] 学习
pattern = re.compile('[12345679]')
# [a-z]  [A-Z]  [a-cx-z]  [aeiou]
ret = pattern.search('今天是星期-')
print(ret.group())
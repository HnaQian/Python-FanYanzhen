# a = r'i love \nbaby'
# print(a)
# 字符串前面加r，代表字符串中的转义字符失去意义，一般用在路径字符串的前面

import re

pattern = re.compile(r'\\d')
ret = pattern.search('我今年\d岁')
print(ret.group())
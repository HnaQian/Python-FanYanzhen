import re

# 指定次数
# pattern = re.compile(r'0{4}[gab]{5}')
# ret = pattern.search('我的体重为weighgt75000ababg')
# print(ret.group())

# pattern = re.compile(r'\d{4,6}[a-z=]{4,7}')
# pattern = re.compile(r'\s{0,}\d{2,}\w{3,8}')
# 123456789klove
# 89klove
# pattern = re.compile(r'\S{0,7}\d{2}[a-z\s=]{3,4}')
# pattern = re.compile(r'\d{2}')
pattern = re.compile(r'\d+[a-z]{3,}\S?')
ret = pattern.search('$#123456789klove==y=ou = you#$')
# ret = pattern.findall('$#123456789klove==y=ou = you#$')
# $#1234567
# 123456789klov
# 报错
# print(ret)
print(ret.group())

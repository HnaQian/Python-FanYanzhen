'''
第4题
a = int(input('请输入第一个数:'))
b = int(input('请输入第二个数:'))

if a >= b:
	print(a)

if a < b:
	print(b)
'''
'''
number = input('请输入一个三位数:')
gewei = number[-1]
shiwei = number[1]
baiwei = number[0]
print('个位为%s,十位数是%s,百位数是%s' % (gewei, shiwei, baiwei))
'''
'''
求个位百位十位
number = int(input('请输入一个三位数:'))
# 123
gewei = number % 10
shiwei = number // 10 % 10
baiwei = number // 100
print('个位为%s,十位数是%s,百位数是%s' % (gewei, shiwei, baiwei))
'''
'''
判断闰年
year = int(input('请输入一个年份:'))
if ((year % 4 == 0) and (year % 100 != 0)) or (year % 400 == 0):
	print(666)
'''
'''
number = int(input('请输入一个数:'))

if (number % 3 == 0) and (number % 5 == 0):
	print(666)
'''

a = int(input('请输入第一个数:'))
b = int(input('请输入第二个数:'))

print('交换前-------')
print('a=%d, b=%d' % (a, b))
# 定义一个临时变量
# c = None
# 先将a的值保存到c中
# c = a
# 将b的值给a
# a = b
# 将c中保存的a的值给b
# b = c

# [a, b] = [b, a]
# (a, b) = (b, a)
a, b = b, a
print('交换后-------')
print('a=%d, b=%d' % (a, b))
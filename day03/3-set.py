# s1 = {100, 200, 300, 't-mac', 'kobe', 't-mac', 'wade', 't-mac', (1, 2, 3), {'name': 'haha'}}

# s1 = {}
# s1 = set()
# print(s1)
# print(type(s1))

s1 = {'林俊杰', '王力宏', '张信哲', '任贤齐', '林宥嘉'}
s2 = {'蔡依林', '皮裤女', '梁静茹', '张信哲', '林俊杰'}

print(s1 | s2)
print(s1 & s2)
print(s1 - s2)
print(s2 - s1)
print(s1 ^ s2)
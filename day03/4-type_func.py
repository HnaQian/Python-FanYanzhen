# a = 153.64
# b = int(a)
# print(b, type(b))
# a = 100
# b = int(input('请输入要相加的数:'))
# c = a + b
# print(c, type(b))


# 字符串的拼接实例
page = 5
url = 'https://www.qiushibaike.com/8hr/page/{}'
# url1 = url + str(page) + '/'
url1 = url.format(page)
print(url1)
# url1 = url % page
# page = 5
# url = 'hshhs://www.ijisdjfijdf.com/sdhf/'
# url1 = url + str(page) + '/'
# print(url1)
# page = 5
# url = 'hshhs://www.ijisdjfijdf.com/sdhf/%d'
# url1 = url.format(page)
# print(url1)


# a = '3.1415'
# a = 100
# c = float(a)
# print(c, type(c))

# a = ' '
# a = None
# print(bool(a))

# a = 100.34
# c = str(a)
# print(c, type(c))

# a = ['梅西', 'c罗', '贝克汉姆', '伊涅斯塔']
# a = ('梅西', 'c罗', '贝克汉姆', '伊涅斯塔')
# a = {'name': '狗蛋', 'age': 30}

# c = str(a)

# print(c, type(c))

# a = (100, 200, 300)
# a = ['hello', 'baby']

# a = [('name', '狗蛋'), ('age', 40), ('wife', '高圆圆')]
# d = dict(a)
# print(d)
# a = 'baby'
# a = 100   # 这个不能转
# a = {3: 'wade', 23: 'jordan', 1: 't-mac', 11: 'yao'}
# a = {100, 200, 300}

# t = tuple(a)
# print(t)
# lt = list(a)
# print(lt)

# a = ['baby', 'hello', 'haha', 'baby', 'baby']
# b = (1, 2, 3)
# c = {'name': 'goudna', 'age': 18}
# s = set(c)
# print(s)